package com.endpoint.alto;
 
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

 public class MainApplication
 {
   private static Logger logger = Logger.getLogger(MainApplication.class);
   
   public static void main(String[] args) {
     ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
     logger.info("ALTO endpoint is started...");
   }
 }
