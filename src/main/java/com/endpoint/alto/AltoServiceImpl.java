 package com.endpoint.alto;
 
 import com.endpoint.alto.hibernate.interfaces.IGeneralAction;
 import com.endpoint.alto.hibernate.interfaces.ILogRequestResponseService;
 import com.endpoint.alto.hibernate.interfaces.IParamBuilder;
 import com.endpoint.alto.hibernate.interfaces.IStatusCodeAction;
 import com.endpoint.alto.hibernate.model.Account;
 import com.endpoint.alto.util.NetworkWorker;
 import com.endpoint.central.interfaces.IRequestService;
import com.endpoint.central.service.util.Constant;
 import com.endpoint.central.service.util.Status;
 import java.util.ArrayList;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 import org.apache.http.NameValuePair;
 import org.apache.log4j.Logger;
 
 public class AltoServiceImpl implements IRequestService
 {
   private IGeneralAction generalAction;
   private IParamBuilder paramBuilder;
   private IStatusCodeAction resultCodeAction;
   private NetworkWorker networkWorker;
   private ILogRequestResponseService requestResponeLogService;
   private Logger logger = Logger.getLogger(AltoServiceImpl.class);
   
   public Map checkBalance(Long processId, String mid, String accountId, String signature) {
	   requestResponeLogService.saveRequestLog(processId, "checkBalance&mid=" + mid + "&accountId=" + accountId + "&signature=" + signature);
     
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.SALDO);
     Map result = new HashMap();
     if (isMerchantSignatureValid) {
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.SALDO);
       List<NameValuePair> params = paramBuilder.buildParamForCheckBalance(mid, accountId, switchNetworkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     }
     else {
       result.put(Constant.KEY_RESULT_CODE,  Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     
 
    return result;
   }
   
 
   public Map login(Long processId, String mid, String accountId, String signature)
   {
    requestResponeLogService.saveRequestLog(processId, "login&mid=" + mid + "&accountId=" + accountId + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.LOGIN);
     Map result = new HashMap();
     if (isMerchantSignatureValid) {
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.LOGIN);
       List<NameValuePair> params = paramBuilder.buildParamForLogin(accountId, mid, switchNetworkSignature);
       
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLReg());
       
       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map registerNewCustomer(Long processId, String mid, String name, String mobileNo, String email, String dateOfBirth, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "registerNewCustomer&mid=" + mid + "&name=" + name + "&email=" + email + "&dob=" + dateOfBirth + "&mobileNo=" + mobileNo + "&signature=" + signature);
     
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, mobileNo, signature, NetworkWorker.Operation.REGISTRATION);
     Long registeredAccountId = generalAction.getAlreadyRegisteredAccountIdByMobileNo(mobileNo);
     
     Map result = new HashMap();
     if (registeredAccountId != null) {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_ACCOUNT_ALREADY_REGISTERED);
       result.put(Constant.KEY_USER_INFO_ACCOUNTID, String.valueOf(registeredAccountId));
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
	 }
     
     if (isMerchantSignatureValid) {
       String switchNetworkSignature = generalAction.buildNetworkSignatureForRegistration(mid, mobileNo, NetworkWorker.Operation.REGISTRATION);
       List<NameValuePair> params = paramBuilder.buildParamForRegistration(name, mobileNo, mid, switchNetworkSignature, email, dateOfBirth);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLReg());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);

       if ((resultCode != null) && (resultCode.equals(Constant.RESULT_CODE_SUCCESS_GENERAL))) {
         Long accountId = Long.valueOf(Long.parseLong(String.valueOf(result.get(Constant.KEY_USER_INFO_ACCOUNTID))));
         generalAction.saveAccount(mid, accountId, mobileNo, email, name, dateOfBirth);
       }
       
 
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     }
     else {
      result.put(Constant.KEY_RESULT_CODE,  Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   
   public Map closeAccount(Long processId, String mid, String accountId, String signature)
   {
    requestResponeLogService.saveRequestLog(processId, "closeAccount&mid=" + mid + "&accountId=" + accountId + "&signature=" + signature);
    Map result = new HashMap();

    Integer accountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountId)));
    if (accountStatus.intValue() == -1) {
      result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_NOT_FOUND);
      requestResponeLogService.saveResponseLog(processId, result);
      return result;
     }
     if (accountStatus.intValue() == 1) {
       result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_ALREADY_CLOSED);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     
     String signatureToCheckBalance = generalAction.generateInternalSignature(mid, accountId, NetworkWorker.Operation.SALDO);
     boolean isSafeToClose = false;
     result = checkBalance(processId, mid, accountId, signatureToCheckBalance);

     String checkBalanceResultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
     if (checkBalanceResultCode.equals(Status.ERROR_INVALID_MIDD_SIGNATURE)) {
       return result;
     }
     
    String balance = String.valueOf(result.get(Constant.KEY_USER_INFO_BALANCE));
    Long balanceParsed = null;
     try {
      balanceParsed = Long.valueOf(Long.parseLong(balance));
     } catch (Exception e) {
       logger.error("can not parse balance. Balance:" + balance);
     }
     
     if ((balanceParsed != null) && (balanceParsed.longValue() > Constant.MIN_AMOUNT_ACCOUNT_CLOSE.longValue())) {
       isSafeToClose = false;
     } else {
       isSafeToClose = true;
     }
     
     result = new HashMap();
     String message = null;
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CLOSEACCOUNT);
     if ((isMerchantSignatureValid) && (isSafeToClose))
     {
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CLOSEACCOUNT);
       List<NameValuePair> params = paramBuilder.buildParamForCloseAccount(accountId, mid, switchNetworkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
       
 
      String resultCode = resultCodeAction.getTranslationStatusCode(result);
      if (resultCode.equals(Constant.RESULT_CODE_SUCCESS_GENERAL)) {
        generalAction.deactivateAccount(Long.valueOf(Long.parseLong(accountId)));
       }
       
      result.put(Constant.KEY_RESULT_CODE, resultCode);
    } else if (!isSafeToClose) {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_MIN_CLOSE_ACCOUNT_AMOUNT_EXIST);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map execChangePhoneNumber(Long processId, String mid, String accountId, String mobileNo, String signature, String newMobileNo)
   {
    requestResponeLogService.saveRequestLog(processId, "execChangePhoneNumber&mid=" + mid + "&accountId=" + accountId + "&mobileNo=" + mobileNo + "&newMobileNo=" + newMobileNo + "&signature=" + signature);

    boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CHANGENOHP);
    Map result = new HashMap();
    String message = null;
    if (isMerchantSignatureValid) {
      String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CHANGENOHP);
      List<NameValuePair> params = paramBuilder.buildParamForChangeMobileNoExecute(accountId, mid, mobileNo, newMobileNo, switchNetworkSignature);

      result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
      String resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));

      if ((resultCode != null) && (resultCode.equals(Constant.RESULT_CODE_SUCCESS_GENERAL))) {
        generalAction.saveNewPhoneNumber(Long.valueOf(Long.parseLong(accountId)), newMobileNo);
       }
       
      result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map inqChangePhoneNumber(Long processId, String mid, String accountId, String mobileNo, String signature, String newMobileNo)
   {
     requestResponeLogService.saveRequestLog(processId, "inqChangePhoneNumber&mid=" + mid + "&accountId=" + accountId + "&mobileNo=" + mobileNo + "&newMobileNo=" + newMobileNo + "&signature=" + signature);

     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CHANGENOHP);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CHANGENOHP);
       List<NameValuePair> params = paramBuilder.buildParamForChangeMobileNoInq(accountId, mid, mobileNo, newMobileNo, switchNetworkSignature);

       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map getMasterData(Long processId)
   {
    requestResponeLogService.saveRequestLog(processId, "getMasterData");
    Map result = new HashMap();
    List<NameValuePair> params = new ArrayList();
     try {
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLData());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } catch (Exception e) {
       logger.error("getMasterData", e);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map inqUserAsRegistered(Long processId, String mid, String accountId, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "inqUserAsRegistered&mid=" + mid + "&accountId=" + accountId + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CHANGEPRODUCT);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       Integer accountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountId)));
       Short accountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));

       if (accountStatus.intValue() == -1) {
         result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_NOT_FOUND);
         requestResponeLogService.saveResponseLog(processId, result);
         return result;
       }
      if (accountStatus.intValue() == 1) {
        result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_ALREADY_CLOSED);
        requestResponeLogService.saveResponseLog(processId, result);
        return result;
       }
       
       if (accountType == Account.ACCOUNT_TYPE_UNREGISTERD) {
         String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CHANGEPRODUCT);
         List<NameValuePair> params = paramBuilder.buildParamForUpgradeUserInq(mid, accountId, switchNetworkSignature);
         result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
         
         String resultCode = resultCodeAction.getTranslationStatusCode(result);
         result.put(Constant.KEY_RESULT_CODE, resultCode);
       } else {
         result.put(Constant.KEY_RESULT_CODE, Status.ERROR_ACCOUNT_NOT_AS_UNREGISTERED);
       }
     }
     else
     {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map execUserAsRegistered(Long processId, String mid, String accountId, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "execUserAsRegistered&mid=" + mid + "&accountId=" + accountId + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CHANGEPRODUCT);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       Integer accountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountId)));
       Short accountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));
       if (accountStatus.intValue() == -1) {
         result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_NOT_FOUND);
         requestResponeLogService.saveResponseLog(processId, result);
         return result;
       }
       if (accountStatus.intValue() == 1) {
         result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_ALREADY_CLOSED);
         requestResponeLogService.saveResponseLog(processId, result);
         return result;
       }
       if (accountType == Account.ACCOUNT_TYPE_UNREGISTERD) {
         String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CHANGEPRODUCT);

         List<NameValuePair> params = paramBuilder.buildParamForUpgradeUserExec(mid, accountId, switchNetworkSignature);
         result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

         String resultCode = resultCodeAction.getTranslationStatusCode(result);
         if (resultCode.equals(Constant.RESULT_CODE_SUCCESS_GENERAL)) {
           generalAction.setUserAsRegister(Long.valueOf(Long.parseLong(accountId)));
         }
         
 
        result.put(Constant.KEY_RESULT_CODE, resultCode);
       }
       else {
         result.put(Constant.KEY_RESULT_CODE, Status.ERROR_ACCOUNT_NOT_AS_UNREGISTERED);
       }
     }
     else
     {	
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
 
   public Map execP2PTransaction(Long processId, String mid, String accountId, String signature, String accountIdDest, Long amount, String note)
   {
     requestResponeLogService.saveRequestLog(processId, "execP2PTransaction&mid=" + mid + "&accountId=" + accountId + "&accountIdDest=" + accountIdDest + "&amount=" + amount + "&signature=" + signature);

     Map result = new HashMap();
     Integer remitterAccountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountId)));
     Integer beneficiaryAccountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountIdDest)));
     Short remitterAccountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));
     if (remitterAccountStatus.intValue() == -1) {
       result.put(Constant.KEY_RESULT_CODE, Status.REM_ACCOUNT_NOT_FOUND);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     if (remitterAccountStatus.intValue() == 1) {
       result.put(Constant.KEY_RESULT_CODE, Status.REM_ACCOUNT_ALREADY_CLOSED);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     
     if (beneficiaryAccountStatus.intValue() == -1) {
       result.put(Constant.KEY_RESULT_CODE, Status.BENEF_ACCOUNT_NOT_FOUND);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     if (beneficiaryAccountStatus.intValue() == 1) {
       result.put(Constant.KEY_RESULT_CODE, Status.BENEF_ACCOUNT_ALREADY_CLOSED);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     
     if (remitterAccountType == Account.ACCOUNT_TYPE_UNREGISTERD) {
       result.put(Constant.KEY_RESULT_CODE, Status.REMMITER_ACCOUNT_TYPE_NOT_ALLOWED);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     
    boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.TRANSFERTRANS);
    String message = null;
    if (isMerchantSignatureValid) {
      String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.TRANSFERTRANS);
      List<NameValuePair> params = paramBuilder.buildParamForP2PExecutePayment(mid, accountId, switchNetworkSignature, amount, accountIdDest, note);
      result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

      String resultCode = resultCodeAction.getTranslationStatusCode(result);
      if (resultCode.equals(Status.SUCCESS)) {}



      result.put(Constant.KEY_RESULT_CODE, resultCode);
     }
     else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
 
   public Map inquiryP2PTransaction(Long processId, String mid, String accountId, String signature, String accountIdDest, Long amount, String note)
   {
     requestResponeLogService.saveRequestLog(processId, "inquiryP2PTransaction&mid=" + mid + "&accountId=" + accountId + "&accountIdDest=" + accountIdDest + "&amount=" + amount + "&signature=" + signature);

     Map result = new HashMap();
     Integer remitterAccountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountId)));
     Integer beneficiaryAccountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountIdDest)));
     Short remitterAccountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));
     if (remitterAccountStatus.intValue() == -1) {
       result.put(Constant.KEY_RESULT_CODE, Status.REM_ACCOUNT_NOT_FOUND);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     if (remitterAccountStatus.intValue() == 1) {
       result.put(Constant.KEY_RESULT_CODE, Status.REM_ACCOUNT_ALREADY_CLOSED);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     
     if (beneficiaryAccountStatus.intValue() == -1) {
       result.put(Constant.KEY_RESULT_CODE, Status.BENEF_ACCOUNT_NOT_FOUND);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     if (beneficiaryAccountStatus.intValue() == 1) {
       result.put(Constant.KEY_RESULT_CODE, Status.BENEF_ACCOUNT_ALREADY_CLOSED);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     logger.info("ACCOUNT TYPE REMMITER:" + remitterAccountType);
     if (remitterAccountType == Account.ACCOUNT_TYPE_UNREGISTERD) {
       result.put(Constant.KEY_RESULT_CODE, Status.REMMITER_ACCOUNT_TYPE_NOT_ALLOWED);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
     }
     
    boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.TRANSFERINQ);
    String message = null;
    if (isMerchantSignatureValid) {
      String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.TRANSFERINQ);
      List<NameValuePair> params = paramBuilder.buildParamForP2PInqPayment(mid, accountId, switchNetworkSignature, amount, accountIdDest, note);
      result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

      String resultCode = resultCodeAction.getTranslationStatusCode(result);
      result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map getTransaction(Long processId, String mid, String accountid, String dateFrom, String dateTo, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "getTransaction&mid=" + mid + "&accountId=" + accountid + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo + "&signature=" + signature);

     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountid, signature, NetworkWorker.Operation.LASTTRANS);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid)
     {
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountid, NetworkWorker.Operation.LASTTRANS);
       List<NameValuePair> params = paramBuilder.buildParamForGetTransaction(mid, accountid, dateFrom, dateTo, switchNetworkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     }
     else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map inqP2MTransaction(Long processId, String mid, String accountId, String destAccountId, Long amount, String note, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "processId&mid=" + mid + "&accountId=" + accountId + "&destAccountId=" + destAccountId + "&amount=" + amount + "&signature=" + signature);
     Map result = new HashMap();

     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.TRANSFERMERCHANTINQ);
     boolean isDestAccountMerchant = generalAction.isAccountMerchant(destAccountId);
     if (!isDestAccountMerchant) {
       result.put(Constant.KEY_RESULT_CODE, Status.DESTACCOUNT_NOT_MERCHANT);
       requestResponeLogService.saveResponseLog(processId, result);
       return result;
	 }
     String message = null;
     if (isMerchantSignatureValid) {
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.TRANSFERMERCHANTINQ);
       List<NameValuePair> params = paramBuilder.buildParamForInqMultiChanelTransfer(mid, accountId, switchNetworkSignature, amount, destAccountId, note);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
	}
   
   
   public Map execP2MTransaction(Long processId, String mid, String accountId, String destAccountId, Long amount, String note, String signature)
   {
    requestResponeLogService.saveRequestLog(processId, "execP2MTransaction&mid=" + mid + "&accountId=" + accountId + "&destAccountId=" + destAccountId + "&amount=" + amount + "&signature=" + signature);
    Map result = new HashMap();
    boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.TRANSFERMERCHANTTRANS);
    boolean isAccountMerchant = generalAction.isAccountMerchant(destAccountId);
    if (!isAccountMerchant) {
      result.put(Constant.KEY_RESULT_CODE, Status.DESTACCOUNT_NOT_MERCHANT);
      requestResponeLogService.saveResponseLog(processId, result);
      return result;
     }
     
    String message = null;
    if (isMerchantSignatureValid) {
      String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.TRANSFERMERCHANTTRANS);
      List<NameValuePair> params = paramBuilder.buildParamForExecMultiChanelTransfer(mid, accountId, switchNetworkSignature, amount, destAccountId, note);
      result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

      String resultCode = resultCodeAction.getTranslationStatusCode(result);
      result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map inqUserAsMerchant(Long processId, String mid, String accountId, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "inqUserAsMerchant&mid=" + mid + "&accountId=" + accountId + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CHANGEPRODUCTMERCHANT);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       Integer accountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountId)));
       Short accountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));
       
       if (accountStatus.intValue() == -1) {
         result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_NOT_FOUND);
         requestResponeLogService.saveResponseLog(processId, result);
         return result;
       }
       if (accountStatus.intValue() == 1) {
         result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_ALREADY_CLOSED);
         requestResponeLogService.saveResponseLog(processId, result);
         return result;
       }
       
       if(accountType == Account.ACCOUNT_TYPE_MERCHANT){
    	   result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_ALREADY_MERCHANT);
           requestResponeLogService.saveResponseLog(processId, result);
           return result;
       }
       
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CHANGEPRODUCTMERCHANT);
       List<NameValuePair> params = paramBuilder.buildParamForUpgradeUserMerchantInq(mid, accountId, switchNetworkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
      logger.info("set user registered request is error due to invalid signature");
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map execUserAsMerchant(Long processId, String mid, String accountId, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "inqUserAsMerchant&mid=" + mid + "&accountId=" + accountId + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CHANGEPRODUCTMERCHANT);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       Integer accountStatus = generalAction.getAccountStatus(Long.valueOf(Long.parseLong(accountId)));
       Short accountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));

       if (accountStatus.intValue() == -1) {
           result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_NOT_FOUND);
           requestResponeLogService.saveResponseLog(processId, result);
           return result;
         }
         if (accountStatus.intValue() == 1) {
           result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_ALREADY_CLOSED);
           requestResponeLogService.saveResponseLog(processId, result);
           return result;
         }
       if(accountType == Account.ACCOUNT_TYPE_MERCHANT){
    	   result.put(Constant.KEY_RESULT_CODE, Status.ACCOUNT_ALREADY_MERCHANT);
           requestResponeLogService.saveResponseLog(processId, result);
           return result;
       }
 
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CHANGEPRODUCTMERCHANT);
       List<NameValuePair> params = paramBuilder.buildParamForUpgradeUserMerchantExec(mid, accountId, switchNetworkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       if (resultCode.equals(Status.SUCCESS)) {
         generalAction.setUserAsMerchant(Long.valueOf(Long.parseLong(accountId)));
       }
       
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map inqBankTransfer(Long processId, String mid, String accountId, String bankId, String accountNo, Long amount, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "inqBankTransfer&mid=" + mid + "&accountId=" + accountId + "&bankId=" + bankId + "&accountNo=" + accountNo + "&amount=" + amount + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.TRANSFERBANKINQ);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       Short remitterAccountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));
       
       if(remitterAccountType == Account.ACCOUNT_TYPE_UNREGISTERD){
           result.put(Constant.KEY_RESULT_CODE, Status.REMMITER_ACCOUNT_TYPE_NOT_ALLOWED);
           requestResponeLogService.saveResponseLog(processId, result);
           return result;
       }
       
       String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.TRANSFERBANKINQ);
       List<NameValuePair> params = paramBuilder.buildParamForBankTransferInq(mid, accountId, amount, bankId, accountNo, switchNetworkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map execBankTransfer(Long processId, String mid, String accountId, String bankId, String accountNo, Long amount, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "execBankTransfer&mid=" + mid + "&accountId=" + accountId + "&bankId=" + bankId + "&accountNo=" + accountNo + "&amount=" + amount + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.TRANSFERBANKTRANS);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid)
     {
         Short remitterAccountType = generalAction.getAccountType(Long.valueOf(Long.parseLong(accountId)));
         
         if(remitterAccountType == Account.ACCOUNT_TYPE_UNREGISTERD){
             result.put(Constant.KEY_RESULT_CODE, Status.REMMITER_ACCOUNT_TYPE_NOT_ALLOWED);
             requestResponeLogService.saveResponseLog(processId, result);
             return result;
         }
         
       String inquirySignature = generalAction.buildMerchantSignature(mid, accountId, NetworkWorker.Operation.TRANSFERBANKINQ);
       result = inqBankTransfer(processId, mid, accountId, bankId, accountNo, amount, inquirySignature);
       String inqResultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));

       if (inqResultCode.equals(Status.SUCCESS)) {
         String switchNetworkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.TRANSFERBANKTRANS);
         List<NameValuePair> params = paramBuilder.buildParamForBankTransferExec(mid, accountId, amount, bankId, accountNo, switchNetworkSignature);
         result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
         String resultCode = resultCodeAction.getTranslationStatusCode(result);
         result.put(Constant.KEY_RESULT_CODE, resultCode);
       } else {
         result.put(Constant.KEY_RESULT_CODE, Status.ERROR_BANKTRX_EXEC);
       }
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map updateUserProfile(Long processId, String mid, String name, String accountId, String email, String dob, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "updateUserProfile&mid=" + mid + "&accountId=" + accountId + "&email=" + email + "&dob=" + dob + "&name=" + name + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountId, signature, NetworkWorker.Operation.CHANGEDATAUSER);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       String networkSignature = generalAction.buildNetworkSignature(mid, accountId, NetworkWorker.Operation.CHANGEDATAUSER);
       List<NameValuePair> params = paramBuilder.buildParamForUpdateUserProfile(mid, accountId, name, email, dob, networkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());

       String resultCode = resultCodeAction.getTranslationStatusCode(result);

       if (resultCode.equals(Status.SUCCESS)) {
         generalAction.saveUpdateAccount(Long.valueOf(Long.parseLong(accountId)), name, email, dob);
       }
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map notifyBalance(Long processId, String mid, String accountId, Long balance) {
     Long resultCode = requestResponeLogService.saveRequestLog(processId, "&mid=" + mid + "&accountId=" + accountId + "&balance=" + String.valueOf(balance));
     List<NameValuePair> params = paramBuilder.buildNotifyBalance(mid, accountId, balance);
     Map result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndPointNotifyBalance());
     if (String.valueOf(resultCode).equals(Status.SAVE_SUCCESS)) {
       result.put(Constant.KEY_RESULT_CODE, Status.SUCCESS);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.FAILURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     
     return result;
   }
   
 
   public Map inqTopupPulsa(Long processId, String mid, String accountid, String nohpdest, Long amount, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "inqTopupPulsa&mid=" + mid + "&accountId=" + accountid + "&nohpdest=" + nohpdest + "&amount=" + amount + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountid, signature, NetworkWorker.Operation.TOPUPOTHERINQ);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
       String networkSignature = generalAction.buildNetworkSignature(mid, accountid, NetworkWorker.Operation.TOPUPOTHERINQ);
       List<NameValuePair> params = paramBuilder.buildParamForInqTopUpSelf(mid, accountid, nohpdest, amount, networkSignature);
       result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
       
       String resultCode = resultCodeAction.getTranslationStatusCode(result);
       result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
       result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
     requestResponeLogService.saveResponseLog(processId, result);
     return result;
   }
   
   public Map execTopupPulsa(Long processId, String mid, String accountid, String nohpdest, Long amount, String signature)
   {
     requestResponeLogService.saveRequestLog(processId, "execTopupPulsa&mid=" + mid + "&accountId=" + accountid + "&nohpdest=" + nohpdest + "&amount=" + amount + "&signature=" + signature);
     boolean isMerchantSignatureValid = generalAction.isSignatureValid(mid, accountid, signature, NetworkWorker.Operation.TOPUPOTHERTRANS);
     Map result = new HashMap();
     String message = null;
     if (isMerchantSignatureValid) {
      String networkSignature = generalAction.buildNetworkSignature(mid, accountid, NetworkWorker.Operation.TOPUPOTHERTRANS);
      List<NameValuePair> params = paramBuilder.buildParamForExecTopUpSelf(mid, accountid, nohpdest, amount, networkSignature);
      result = networkWorker.sendRequest(processId, params, NetworkWorker.getEndpointURLService());
       
		String resultCode = resultCodeAction.getTranslationStatusCode(result);
		result.put(Constant.KEY_RESULT_CODE, resultCode);
     } else {
      result.put(Constant.KEY_RESULT_CODE, Status.ERROR_INVALID_MIDD_SIGNATURE);
     }
    requestResponeLogService.saveResponseLog(processId, result);
    return result;
   }
   
   public IGeneralAction getGeneralAction() {
    return generalAction;
   }
   
   public void setGeneralAction(IGeneralAction generalAction) {
    this.generalAction = generalAction;
   }
   
   public IParamBuilder getParamBuilder() {
    return paramBuilder;
   }
   
   public void setParamBuilder(IParamBuilder paramBuilder) {
   this.paramBuilder = paramBuilder;
   }
   
   public IStatusCodeAction getResultCodeAction()
   {
  return resultCodeAction;
   }
   
   public void setResultCodeAction(IStatusCodeAction resultCodeAction)
   {
   this.resultCodeAction = resultCodeAction;
   }
   
   public NetworkWorker getNetworkWorker()
   {
   return networkWorker;
   }
   
   public void setNetworkWorker(NetworkWorker networkWorker)
   {
   this.networkWorker = networkWorker;
   }
   
   public ILogRequestResponseService getRequestResponeLogService()
   {
    return requestResponeLogService;
   }
   
   public void setRequestResponeLogService(ILogRequestResponseService requestResponeLogService)
   {
   this.requestResponeLogService = requestResponeLogService;
   }
 }

