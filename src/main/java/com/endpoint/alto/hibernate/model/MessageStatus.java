/*    */ package com.endpoint.alto.hibernate.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ public class MessageStatus
/*    */   implements Serializable
/*    */ {
/*    */   private static final long serialVersionUID = 4925902564084901553L;
/*    */   private Long messageStatusId;
/*    */   private String messageEn;
/*    */   private String messageId;
/*    */   
/*    */   public Long getMessageStatusId()
/*    */   {
/* 15 */     return messageStatusId;
/*    */   }
/*    */   
/* 18 */   public void setMessageStatusId(Long messageStatusId) { this.messageStatusId = messageStatusId; }
/*    */   
/*    */   public String getMessageEn() {
/* 21 */     return messageEn;
/*    */   }
/*    */   
/* 24 */   public void setMessageEn(String messageEn) { this.messageEn = messageEn; }
/*    */   
/*    */   public String getMessageId() {
/* 27 */     return messageId;
/*    */   }
/*    */   
/* 30 */   public void setMessageId(String messageId) { this.messageId = messageId; }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.model.MessageStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */