/*    */ package com.endpoint.alto.hibernate.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ import java.util.Date;
/*    */ 
/*    */ public class RequestResponseLog
/*    */   implements Serializable
/*    */ {
/*    */   private static final long serialVersionUID = -5769827606186683678L;
/*    */   private Long logId;
/*    */   private Date requestIn;
/*    */   private String requestContent;
/*    */   private String responseContent;
/*    */   private Date responseOut;
/*    */   
/*    */   public Long getLogId()
/*    */   {
/* 18 */     return logId;
/*    */   }
/*    */   
/* 21 */   public void setLogId(Long logId) { this.logId = logId; }
/*    */   
/*    */   public Date getRequestIn() {
/* 24 */     return requestIn;
/*    */   }
/*    */   
/* 27 */   public void setRequestIn(Date requestIn) { this.requestIn = requestIn; }
/*    */   
/*    */   public String getRequestContent() {
/* 30 */     return requestContent;
/*    */   }
/*    */   
/* 33 */   public void setRequestContent(String requestContent) { this.requestContent = requestContent; }
/*    */   
/*    */   public String getResponseContent() {
/* 36 */     return responseContent;
/*    */   }
/*    */   
/* 39 */   public void setResponseContent(String responseContent) { this.responseContent = responseContent; }
/*    */   
/*    */   public Date getResponseOut() {
/* 42 */     return responseOut;
/*    */   }
/*    */   
/* 45 */   public void setResponseOut(Date responseOut) { this.responseOut = responseOut; }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.model.RequestResponseLog
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */