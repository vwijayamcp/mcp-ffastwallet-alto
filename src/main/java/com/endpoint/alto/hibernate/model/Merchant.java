/*    */ package com.endpoint.alto.hibernate.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Merchant
/*    */   implements Serializable
/*    */ {
/*    */   private static final long serialVersionUID = 3033191355614702879L;
/*    */   private Long merchantId;
/*    */   private String merchantName;
/*    */   private String key_merchant;
/*    */   private MerchantNetwork merchantNetwork;
/*    */   
/*    */   public Long getMerchantId()
/*    */   {
/* 18 */     return merchantId;
/*    */   }
/*    */   
/* 21 */   public void setMerchantId(Long merchantId) { this.merchantId = merchantId; }
/*    */   
/*    */   public String getMerchantName() {
/* 24 */     return merchantName;
/*    */   }
/*    */   
/* 27 */   public void setMerchantName(String merchantName) { this.merchantName = merchantName; }
/*    */   
/*    */   public String getKey_merchant() {
/* 30 */     return key_merchant;
/*    */   }
/*    */   
/* 33 */   public void setKey_merchant(String key_merchant) { this.key_merchant = key_merchant; }
/*    */   
/*    */   public MerchantNetwork getMerchantNetwork() {
/* 36 */     return merchantNetwork;
/*    */   }
/*    */   
/* 39 */   public void setMerchantNetwork(MerchantNetwork merchantNetwork) { this.merchantNetwork = merchantNetwork; }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.model.Merchant
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */