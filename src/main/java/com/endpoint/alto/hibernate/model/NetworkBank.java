/*    */ package com.endpoint.alto.hibernate.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class NetworkBank
/*    */   implements Serializable
/*    */ {
/*    */   private static final long serialVersionUID = 9066522061283597320L;
/*    */   private Long networkId;
/*    */   private String networkName;
/*    */   
/*    */   public Long getNetworkId()
/*    */   {
/* 16 */     return networkId;
/*    */   }
/*    */   
/* 19 */   public void setNetworkId(Long networkId) { this.networkId = networkId; }
/*    */   
/*    */   public String getNetworkName() {
/* 22 */     return networkName;
/*    */   }
/*    */   
/* 25 */   public void setNetworkName(String networkName) { this.networkName = networkName; }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.model.NetworkBank
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */