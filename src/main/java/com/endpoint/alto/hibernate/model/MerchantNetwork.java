/*    */ package com.endpoint.alto.hibernate.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ public class MerchantNetwork
/*    */   implements Serializable
/*    */ {
/*    */   private static final long serialVersionUID = -8173002705038782923L;
/*    */   private Long merchant_network_id;
/*    */   private Merchant merchant;
/*    */   private NetworkBank networkBank;
/*    */   private String key_network;
/*    */   
/*    */   public Long getMerchant_network_id()
/*    */   {
/* 17 */     return merchant_network_id;
/*    */   }
/*    */   
/* 20 */   public void setMerchant_network_id(Long merchant_network_id) { this.merchant_network_id = merchant_network_id; }
/*    */   
/*    */   public Merchant getMerchant() {
/* 23 */     return merchant;
/*    */   }
/*    */   
/* 26 */   public void setMerchant(Merchant merchant) { this.merchant = merchant; }
/*    */   
/*    */   public NetworkBank getNetworkBank() {
/* 29 */     return networkBank;
/*    */   }
/*    */   
/* 32 */   public void setNetworkBank(NetworkBank networkBank) { this.networkBank = networkBank; }
/*    */   
/*    */   public String getKey_network() {
/* 35 */     return key_network;
/*    */   }
/*    */   
/* 38 */   public void setKey_network(String key_network) { this.key_network = key_network; }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.model.MerchantNetwork
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */