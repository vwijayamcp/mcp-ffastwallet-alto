/*    */ package com.endpoint.alto.hibernate.model;
/*    */ 
/*    */ import java.io.Serializable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class StatusCode
/*    */   implements Serializable
/*    */ {
/*    */   private static final long serialVersionUID = 8576652641764654392L;
/*    */   private Long statusCodeId;
/*    */   private Long altoCode;
/*    */   private Long middlewareStatusCode;
/*    */   private String statusDescription;
/*    */   
/*    */   public Long getStatusCodeId()
/*    */   {
/* 22 */     return statusCodeId;
/*    */   }
/*    */   
/* 25 */   public void setStatusCodeId(Long statusCodeId) { this.statusCodeId = statusCodeId; }
/*    */   
/*    */   public Long getAltoCode() {
/* 28 */     return altoCode;
/*    */   }
/*    */   
/* 31 */   public void setAltoCode(Long altoCode) { this.altoCode = altoCode; }
/*    */   
/*    */   public Long getMiddlewareStatusCode() {
/* 34 */     return middlewareStatusCode;
/*    */   }
/*    */   
/* 37 */   public void setMiddlewareStatusCode(Long middlewareStatusCode) { this.middlewareStatusCode = middlewareStatusCode; }
/*    */   
/*    */   public String getStatusDescription() {
/* 40 */     return statusDescription;
/*    */   }
/*    */   
/* 43 */   public void setStatusDescription(String statusDescription) { this.statusDescription = statusDescription; }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.model.StatusCode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */