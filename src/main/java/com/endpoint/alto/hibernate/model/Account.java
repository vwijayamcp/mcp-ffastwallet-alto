 package com.endpoint.alto.hibernate.model;
 
 import java.io.Serializable;
 import java.util.Date;
 
 
 
 
 public class Account
   implements Serializable
 {
   private static final long serialVersionUID = 264752403517674808L;
   private Merchant merchant;
   private Long accountId;
   private String mobileNo;
   private Short type;
   private Boolean isActive;
   private String email;
   private String fullname;
   private Date dateOfBirth;
   public static short ACCOUNT_TYPE_UNREGISTERD = 1;
   public static short ACCOUNT_TYPE_REGISTERED = 2;
   public static short ACCOUNT_TYPE_MERCHANT = 3;
   
   public Long getAccountId() {
     return accountId;
   }
   
   public void setAccountId(Long accountId) {
     this.accountId = accountId;
   }
   
   public Merchant getMerchant() { return merchant; }
   
   public void setMerchant(Merchant merchant) {
     this.merchant = merchant;
   }
   
   public String getMobileNo() { return mobileNo; }
   
   public void setMobileNo(String mobileNo) {
     this.mobileNo = mobileNo;
   }
   
   public Short getType() { return type; }
   
   public void setType(Short type)
   {
     this.type = type;
   }
   
   public Boolean getIsActive() {
     return isActive;
   }
   
   public void setIsActive(Boolean isActive) {
     this.isActive = isActive;
   }
   
   public String getEmail() {
     return email;
   }
   
   public void setEmail(String email) {
     this.email = email;
   }
   
   public String getFullname() {
     return fullname;
   }
   
   public void setFullname(String fullname) {
     this.fullname = fullname;
   }
   
   public Date getDateOfBirth() {
     return dateOfBirth;
   }
   
   public void setDateOfBirth(Date dateOfBirth) {
     this.dateOfBirth = dateOfBirth;
   }
 }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.model.Account
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */