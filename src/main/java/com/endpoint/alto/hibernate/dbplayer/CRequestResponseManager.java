/*    */ package com.endpoint.alto.hibernate.dbplayer;
/*    */ 
/*    */ import com.endpoint.alto.hibernate.model.RequestResponseLog;
/*    */ import java.util.List;
/*    */ import org.apache.commons.logging.Log;
/*    */ import org.hibernate.Session;
/*    */ import org.hibernate.SessionFactory;
/*    */ import org.hibernate.Transaction;
/*    */ import org.springframework.orm.hibernate3.HibernateTemplate;
/*    */ import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
/*    */ 
/*    */ public class CRequestResponseManager extends HibernateDaoSupport implements com.endpoint.alto.hibernate.interfaces.ILogRequestResponseManager
/*    */ {
/*    */   public Integer saveRequestLog(RequestResponseLog log)
/*    */   {
/* 16 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/* 17 */     Transaction trx = session.beginTransaction();
/*    */     try {
/* 19 */       session.save(log);
/* 20 */       trx.commit();
/* 21 */       return Integer.valueOf(0);
/*    */     } catch (Exception e) {
/* 23 */       logger.error("saveRequestLog", e);
/* 24 */       trx.rollback();
/* 25 */       return Integer.valueOf(-1);
/*    */     } finally {
/* 27 */       if (session.isOpen()) session.close();
/*    */     }
/*    */   }
/*    */   
/*    */   public RequestResponseLog getRequestLogById(Long requestId)
/*    */   {
/*    */     try {
/* 34 */       String hql = "SELECT r FROM RequestResponseLog r WHERE r.logId = ?";
/* 35 */       Object[] args = { requestId };
/* 36 */       List result = getHibernateTemplate().find(hql, args);
/* 37 */       if (result.size() > 0) {
/* 38 */         return (RequestResponseLog)result.get(0);
/*    */       }
/* 40 */       return null;
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/* 44 */       logger.error("saveResponseLog", e); }
/* 45 */     return null;
/*    */   }
/*    */   
/*    */   public Integer saveResponseLog(RequestResponseLog reqLog)
/*    */   {
/* 50 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/* 51 */     Transaction trx = session.beginTransaction();
/*    */     try {
/* 53 */       session.saveOrUpdate(reqLog);
/* 54 */       trx.commit();
/* 55 */       return Integer.valueOf(0);
/*    */     } catch (Exception e) {
/* 57 */       logger.error("saveResponseLog", e);
/* 58 */       trx.rollback();
/* 59 */       return Integer.valueOf(-1);
/*    */     } finally {
/* 61 */       if (session.isOpen()) session.close();
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.dbplayer.CRequestResponseManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */