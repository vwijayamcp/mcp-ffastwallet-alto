/*    */ package com.endpoint.alto.hibernate.dbplayer;
/*    */ 
/*    */ import com.endpoint.alto.hibernate.interfaces.IStatusCodeDBManager;
/*    */ import com.endpoint.alto.hibernate.model.StatusCode;
/*    */ import java.util.List;
/*    */ import org.apache.commons.logging.Log;
/*    */ import org.springframework.orm.hibernate3.HibernateTemplate;
/*    */ 
/*    */ public class ResultCodeManager extends org.springframework.orm.hibernate3.support.HibernateDaoSupport implements IStatusCodeDBManager
/*    */ {
/*    */   public StatusCode getStatusCodeByAltoCode(Long altoCode)
/*    */   {
/*    */     try
/*    */     {
/* 15 */       String hql = "SELECT s FROM StatusCode s WHERE s.altoCode = ?";
/* 16 */       Object[] args = { altoCode };
/* 17 */       List result = getHibernateTemplate().find(hql, args);
/* 18 */       if (result.size() > 0) {
/* 19 */         return (StatusCode)result.get(0);
/*    */       }
/* 21 */       return null;
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/* 25 */       logger.error("getStatusCodeByAltoCode", e); }
/* 26 */     return null;
/*    */   }
/*    */   
/*    */   public StatusCode getStatusCodeByErrorCode(Long errorCode)
/*    */   {
/*    */     try {
/* 32 */       String hql = "SELECT s FROM StatusCode s WHERE s.statusCodeId = ?";
/* 33 */       Object[] args = { errorCode };
/* 34 */       List result = getHibernateTemplate().find(hql, args);
/* 35 */       if (result.size() > 0) {
/* 36 */         return (StatusCode)result.get(0);
/*    */       }
/* 38 */       return null;
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/* 42 */       logger.error("getStatusCodeByErrorCode", e); }
/* 43 */     return null;
/*    */   }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.dbplayer.ResultCodeManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */