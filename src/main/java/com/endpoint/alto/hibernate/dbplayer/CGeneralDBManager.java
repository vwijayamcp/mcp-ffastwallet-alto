/*     */ package com.endpoint.alto.hibernate.dbplayer;
/*     */ 
/*     */ import com.endpoint.alto.hibernate.model.Account;
/*     */ import com.endpoint.alto.hibernate.model.Merchant;
/*     */ import com.endpoint.alto.hibernate.model.MerchantNetwork;
/*     */ import com.endpoint.alto.hibernate.model.NetworkBank;
/*     */ import java.util.List;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.hibernate.Session;
/*     */ import org.hibernate.SessionFactory;
/*     */ import org.hibernate.Transaction;
/*     */ import org.springframework.orm.hibernate3.HibernateTemplate;
/*     */ import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
/*     */ 
/*     */ public class CGeneralDBManager extends HibernateDaoSupport implements com.endpoint.alto.hibernate.interfaces.IGeneralDBManager
/*     */ {
/*  17 */   private Logger logger = Logger.getLogger(CGeneralDBManager.class);
/*     */   
/*     */   public int saveMerchant(Merchant merchant)
/*     */   {
/*  21 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/*  22 */     Transaction trx = session.beginTransaction();
/*     */     try {
/*  24 */       session.saveOrUpdate(merchant);
/*  25 */       trx.commit();
/*  26 */       return 0;
/*     */     } catch (Exception e) {
/*  28 */       logger.error("saveMerchant", e);
/*  29 */       trx.rollback();
/*  30 */       return -1;
/*     */     } finally {
/*  32 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public Merchant getMerchantById(Long merchantId)
/*     */   {
/*     */     try {
/*  39 */       String hql = "SELECT m FROM Merchant m WHERE m.merchantId = ?";
/*  40 */       Object[] args = { merchantId };
/*  41 */       List result = getHibernateTemplate().find(hql, args);
/*  42 */       if (result.size() > 0) {
/*  43 */         return (Merchant)result.get(0);
/*     */       }
/*  45 */       return null;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/*  49 */       logger.error("getMerchantById", e); }
/*  50 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public int deleteMerchant(Merchant merchant)
/*     */   {
/*  57 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/*  58 */     Transaction trx = session.beginTransaction();
/*     */     try {
/*  60 */       session.delete(merchant);
/*  61 */       trx.commit();
/*  62 */       return 0;
/*     */     } catch (Exception e) {
/*  64 */       logger.error("deleteMerchant", e);
/*  65 */       trx.rollback();
/*  66 */       return -1;
/*     */     } finally {
/*  68 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int saveNetwork(NetworkBank network)
/*     */   {
/*  74 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/*  75 */     Transaction trx = session.beginTransaction();
/*     */     try {
/*  77 */       session.saveOrUpdate(network);
/*  78 */       trx.commit();
/*  79 */       return 0;
/*     */     } catch (Exception e) {
/*  81 */       logger.error("saveNetwork", e);
/*  82 */       trx.rollback();
/*  83 */       return -1;
/*     */     } finally {
/*  85 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public NetworkBank getNetworkBankById(Long networkBankId)
/*     */   {
/*     */     try {
/*  92 */       String hql = "SELECT n FROM NetworkBank n WHERE n.networkId = ?";
/*  93 */       Object[] args = { networkBankId };
/*  94 */       List result = getHibernateTemplate().find(hql, args);
/*  95 */       if (result.size() > 0) {
/*  96 */         return (NetworkBank)result.get(0);
/*     */       }
/*  98 */       return null;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 102 */       logger.error("getNetworkBankById", e); }
/* 103 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */   public int deleteNetworkBank(NetworkBank networkBank)
/*     */   {
/* 109 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/* 110 */     Transaction trx = session.beginTransaction();
/*     */     try {
/* 112 */       session.delete(networkBank);
/* 113 */       trx.commit();
/* 114 */       return 0;
/*     */     } catch (Exception e) {
/* 116 */       logger.error("deleteNetworkBank", e);
/* 117 */       trx.rollback();
/* 118 */       return -1;
/*     */     } finally {
/* 120 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int saveMerchantNetwork(MerchantNetwork merchantNetwork)
/*     */   {
/* 126 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/* 127 */     Transaction trx = session.beginTransaction();
/*     */     try {
/* 129 */       session.saveOrUpdate(merchantNetwork);
/* 130 */       trx.commit();
/* 131 */       return 0;
/*     */     } catch (Exception e) {
/* 133 */       logger.error("saveMerchantNetwork", e);
/* 134 */       trx.rollback();
/* 135 */       return -1;
/*     */     } finally {
/* 137 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public List<MerchantNetwork> getAllNetworkByMerchant(Merchant merchant)
/*     */   {
/*     */     try {
/* 144 */       String hql = "SELECT m FROM MerchantNetwork m WHERE m.merchant = ?";
/* 145 */       Object[] args = { merchant };
/* 146 */       List result = getHibernateTemplate().find(hql, args);
/* 147 */       if (result.size() > 0) {
/* 148 */         return result;
/*     */       }
/* 150 */       return null;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 154 */       logger.error("getAllNetworkByMerchant", e); }
/* 155 */     return null;
/*     */   }
/*     */   
/*     */   public List<MerchantNetwork> getAllMerchantByNetwork(NetworkBank networkBank)
/*     */   {
/*     */     try
/*     */     {
/* 162 */       String hql = "SELECT m FROM MerchantNetwork m WHERE m.networkBank = ?";
/* 163 */       Object[] args = { networkBank };
/* 164 */       List result = getHibernateTemplate().find(hql, args);
/* 165 */       if (result.size() > 0) {
/* 166 */         return result;
/*     */       }
/* 168 */       return null;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 172 */       logger.error("getAllMerchantByNetwork", e); }
/* 173 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */   public int deleteMerchantNetwork(MerchantNetwork merchantNetwork)
/*     */   {
/* 179 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/* 180 */     Transaction trx = session.beginTransaction();
/*     */     try {
/* 182 */       session.delete(merchantNetwork);
/* 183 */       trx.commit();
/* 184 */       return 0;
/*     */     } catch (Exception e) {
/* 186 */       logger.error("deleteMerchantNetwork", e);
/* 187 */       trx.rollback();
/* 188 */       return -1;
/*     */     } finally {
/* 190 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public int saveAccount(Account account)
/*     */   {
/* 196 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/* 197 */     Transaction trx = session.beginTransaction();
/*     */     try {
/* 199 */       session.saveOrUpdate(account);
/* 200 */       trx.commit();
/* 201 */       return 0;
/*     */     } catch (Exception e) {
/* 203 */       logger.error("saveAccount", e);
/* 204 */       trx.rollback();
/* 205 */       return -1;
/*     */     } finally {
/* 207 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public Account getAccoutByAccountId(Long accountId)
/*     */   {
/*     */     try {
/* 214 */       String hql = "SELECT a FROM Account a WHERE a.accountId = ?";
/* 215 */       Object[] args = { accountId };
/* 216 */       List result = getHibernateTemplate().find(hql, args);
/* 217 */       if (result.size() > 0) {
/* 218 */         return (Account)result.get(0);
/*     */       }
/* 220 */       return null;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 224 */       logger.error("getAccoutByAccountId", e); }
/* 225 */     return null;
/*     */   }
/*     */   
/*     */   public List<Account> getAllAccountsFromMerchant(Merchant merchant)
/*     */   {
/*     */     try
/*     */     {
/* 232 */       String hql = "SELECT a FROM Account a WHERE a.merchant = ?";
/* 233 */       Object[] args = { merchant };
/* 234 */       List result = getHibernateTemplate().find(hql, args);
/* 235 */       if (result.size() > 0) {
/* 236 */         return result;
/*     */       }
/* 238 */       return null;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 242 */       logger.error("getAllAccountsFromMerchant", e); }
/* 243 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */   public int deleteAccoutById(Long accountId)
/*     */   {
/* 249 */     Account account = getAccoutByAccountId(accountId);
/* 250 */     Session session = getHibernateTemplate().getSessionFactory().openSession();
/* 251 */     Transaction trx = session.beginTransaction();
/*     */     try {
/* 253 */       session.delete(account);
/* 254 */       trx.commit();
/* 255 */       return 0;
/*     */     } catch (Exception e) {
/* 257 */       logger.error("deleteAccoutById", e);
/* 258 */       trx.rollback();
/* 259 */       return -1;
/*     */     } finally {
/* 261 */       if (session.isOpen()) session.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public Account getAccoutByMobileNo(String mobileNo)
/*     */   {
/*     */     try {
/* 268 */       String hql = "SELECT a FROM Account a WHERE a.mobileNo = ? AND a.isActive = true";
/* 269 */       Object[] args = { mobileNo };
/* 270 */       List result = getHibernateTemplate().find(hql, args);
/* 271 */       if (result.size() > 0) {
/* 272 */         return (Account)result.get(0);
/*     */       }
/* 274 */       return null;
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/* 278 */       logger.error("getAllAccountsFromMerchant", e); }
/* 279 */     return null;
/*     */   }
/*     */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.dbplayer.CGeneralDBManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */