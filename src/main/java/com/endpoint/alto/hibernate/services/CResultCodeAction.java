 package com.endpoint.alto.hibernate.services;
 
 import com.endpoint.alto.hibernate.interfaces.IStatusCodeAction;
 import com.endpoint.alto.hibernate.interfaces.IStatusCodeDBManager;
 import com.endpoint.alto.hibernate.model.StatusCode;
import com.endpoint.central.service.util.Constant;
import com.endpoint.central.service.util.Status;

import java.util.Map;
 import org.apache.log4j.Logger;
 
 
 
 public class CResultCodeAction implements IStatusCodeAction
 {
   private IStatusCodeDBManager dbResultCodeManager;
   private Logger logger = Logger.getLogger(CResultCodeAction.class);
   
   public String getTranslationStatusCode(Map map) {
     Object objectKeyResult = map.get(Constant.KEY_RESULT_CODE);
     if (objectKeyResult == null) {
       return Status.ALTO_CODE_NOT_FOUND;
     }
     
     String altoCode = String.valueOf(objectKeyResult);
     
     if ((String.valueOf(altoCode).equals(Constant.RESULT_CODE_SUCCESS)) || (String.valueOf(altoCode).equals(Constant.RESULT_CODE_INQ_SUCCESS)) || 
       (String.valueOf(altoCode).equals(Constant.RESULT_CODE_EXEC_SUCCESS)) || (String.valueOf(altoCode).equals(Constant.RESULT_CODE_SUCCESS_GENERAL)) || 
       (String.valueOf(altoCode).equals(Constant.RESULT_CODE_SUCCESS_UPGRADE))) {
       return Status.SUCCESS;
     }
     
     logger.info("Error code from ALTO:" + altoCode);
     StatusCode registeredCode = dbResultCodeManager.getStatusCodeByAltoCode(Long.valueOf(Long.parseLong(altoCode)));
     
     if (registeredCode == null) { return Status.ALTO_CODE_NOT_FOUND;
     }
     return String.valueOf(registeredCode.getMiddlewareStatusCode());
   }
   
   public IStatusCodeDBManager getDbResultCodeManager() {
     return dbResultCodeManager;
   }
   
   public void setDbResultCodeManager(IStatusCodeDBManager dbResultCodeManager) {
     this.dbResultCodeManager = dbResultCodeManager;
   }
 }

