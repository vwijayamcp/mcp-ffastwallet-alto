package com.endpoint.alto.hibernate.services;

import com.endpoint.alto.hibernate.interfaces.IGeneralDBManager;
import com.endpoint.alto.hibernate.interfaces.IParamBuilder;
import com.endpoint.alto.hibernate.model.Account;
import com.endpoint.alto.hibernate.model.Merchant;
import com.endpoint.alto.util.NetworkWorker;
import com.endpoint.alto.util.NetworkWorker.Operation;
import com.endpoint.central.service.util.Constant;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;


public class CParamBuilder
  implements IParamBuilder
{
   private Logger logger = Logger.getLogger(CParamBuilder.class);
   private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  private IGeneralDBManager dbGeneralManager;
  
  public List<NameValuePair> buildParamForCheckBalance(String mid, String accountId, String signature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));

     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.SALDO.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
    } catch (Exception e) {
       logger.error("buildParamForCheckBalance() with accountId:" + accountId, e);
    }
    
     return params;
  }
  
  public List<NameValuePair> buildParamForLogin(String accountId, String mid, String signature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
    
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.LOGIN.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
    } catch (Exception e) {
       logger.error("buildParamForLogin() with accountId:" + accountId, e);
    }
    
     return params;
  }
  

  public List<NameValuePair> buildParamForRegistration(String name, String mobileNo, String mid, String signature, String email, String dateOfbirth)
  {
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, ""));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_EMAIL, email));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NAME, name));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, mobileNo));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.REGISTRATION.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_DOB, dateOfbirth));
    } catch (Exception e) {
       logger.error("buildParamForRegistration() with mobileNo:" + mobileNo, e);
    }
    
     return params;
  }
  
  public List<NameValuePair> buildParamForCloseAccount(String accountId, String mid, String signature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));

     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CLOSEACCOUNT.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NAME, account.getFullname()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_EMAIL, account.getEmail()));

       String dateOfBirth = dateFormat.format(account.getDateOfBirth());
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_DOB, dateOfBirth));
    } catch (Exception e) {
       logger.error("buildParamForCloseAccount() with accountId:" + accountId, e);
    }
    
     return params;
  }
  

  public List<NameValuePair> buildParamForChangeMobileNoInq(String accountId, String mid, String mobileNo, String newMobileNo, String signature)
  {
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, mobileNo));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NEW_NOHP, newMobileNo));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CHANGENOHP.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_INQ, Constant.INQ));
    } catch (Exception e) {
       logger.error("buildParamForChangeMobileNoInq() with accountId:" + accountId, e);
    }
    
     return params;
  }
  

  public List<NameValuePair> buildParamForChangeMobileNoExecute(String accountId, String mid, String mobileNo, String newMobileNo, String signature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
    
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NEW_NOHP, newMobileNo));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CHANGENOHP.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_INQ, Constant.EXECUTE));
    } catch (Exception e) {
       logger.error("buildParamForChangeMobileNoExecute() with accountId:" + accountId, e);
    }
    
     return params;
  }
  
  public List<NameValuePair> buildParamForUpgradeUserInq(String mid, String accountId, String signature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
    
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_INQ, Constant.INQ));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CHANGEPRODUCT.toString()));
    } catch (Exception e) {
       logger.error("buildParamForChangeMobileNoExecute() with accountId:" + accountId, e);
    }
    
     return params;
  }
  
  public List<NameValuePair> buildParamForUpgradeUserExec(String mid, String accountId, String signature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_INQ, Constant.EXECUTE));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CHANGEPRODUCT.toString()));
    } catch (Exception e) {
       logger.error("buildParamForUpgradeUserExec() with accountId:" + accountId, e);
    }
    
     return params;
  }
  
  public List<NameValuePair> buildParamForP2PInqPayment(String mid, String accountId, String switchNetworkSignature, Long amount, String benefAccountId, String note)
  {
     Account remmitterAccount = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
     Account beneficiaryAccount = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(benefAccountId)));
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, remmitterAccount.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TRANSFERINQ.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MIDBENEF, String.valueOf(beneficiaryAccount.getMerchant().getMerchantId())));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID_DEST, String.valueOf(beneficiaryAccount.getAccountId())));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOTE, note));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
    } catch (Exception e) {
       logger.error("buildParamForP2PInqPayment() with accountId:" + accountId + " and benef account id:" + benefAccountId, e);
    }
    
     return params;
  }
  
  public List<NameValuePair> buildParamForP2PExecutePayment(String mid, String accountId, String switchNetworkSignature, Long amount, String accountIdDest, String note)
  {
     Account remmitterAccount = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
     Account beneficiaryAccount = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountIdDest)));
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, remmitterAccount.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TRANSFERTRANS.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MIDBENEF, String.valueOf(beneficiaryAccount.getMerchant().getMerchantId())));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID_DEST, String.valueOf(beneficiaryAccount.getAccountId())));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOTE, note));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
    } catch (Exception e) {
       logger.error("buildParamForP2PExecutePayment() with accountId:" + accountId + " and benef account id:" + accountIdDest, e);
    }
    
     return params;
  }
  


  public List<NameValuePair> buildParamForGetTransaction(String mid, String accountid, String dateFrom, String dateTo, String switchNetworkSignature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountid)));
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.LASTTRANS.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_STARTDATE, dateFrom));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ENDDATE, dateTo));
    }
    catch (Exception e) {
       logger.error("buildParamForGetTransaction() with mid:" + mid + " and benef account id:" + accountid + ", datefrom:" + dateFormat + ", dateTo:" + dateTo, e);
    }
    
     return params;
  }
  



  public List<NameValuePair> buildParamForInqMultiChanelTransfer(String mid, String accountId, String switchNetworkSignature, Long amount, String destAccountId, String note)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TRANSFERMERCHANTINQ.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MIDBENEF, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID_DEST, destAccountId));
    }
    catch (Exception e) {
       logger.error("buildParamForMultiChanelTransfer() with mid:" + mid + " and account id:" + accountId + ", midDest:" + mid + ", amount:" + amount + ", destMobileNo:, destAccountId:" + destAccountId, e);
    }
     return params;
  }
  


  public List<NameValuePair> buildParamForExecMultiChanelTransfer(String mid, String accountId, String switchNetworkSignature, Long amount, String destAccountId, String note)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TRANSFERMERCHANTTRANS.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MIDBENEF, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID_DEST, destAccountId));
    }
    catch (Exception e) {
       logger.error("buildParamForExecMultiChanelTransfer() with mid:" + mid + " and account id:" + accountId + ", midDest:" + mid + ", amount:" + amount + ", destMobileNo:, destAccountId:" + destAccountId, e);
    }
     return params;
  }
  

  public List<NameValuePair> buildParamForUpgradeUserMerchantInq(String mid, String accountId, String signature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
    
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_INQ, Constant.INQ));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CHANGEPRODUCTMERCHANT.toString()));
    } catch (Exception e) {
       logger.error("buildParamForUpgradeUserMerchantInq() with accountId:" + accountId, e);
    }
    
     return params;
  }
  

  public List<NameValuePair> buildParamForUpgradeUserMerchantExec(String mid, String accountId, String switchNetworkSignature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
    
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_INQ, Constant.EXECUTE));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CHANGEPRODUCTMERCHANT.toString()));
    }
    catch (Exception e) {
       logger.error("buildParamForUpgradeUserMerchantExec() with accountId:" + accountId, e);
    }
    
     return params;
  }
  


  public List<NameValuePair> buildParamForBankTransferInq(String mid, String accountId, Long amount, String bankId, String accountNo, String switchNetworkSignature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
    
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TRANSFERBANKINQ.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_BANKID, bankId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTNO, accountNo));
    } catch (Exception e) {
       logger.error("buildParamForBankTransferInq() with accountId:" + accountId, e);
    }
    
     return params;
  }
  


  public List<NameValuePair> buildParamForBankTransferExec(String mid, String accountId, Long amount, String bankId, String accountNo, String switchNetworkSignature)
  {
     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
    
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHP, account.getMobileNo()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, switchNetworkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TRANSFERBANKTRANS.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_BANKID, bankId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTNO, accountNo));
    } catch (Exception e) {
       logger.error("buildParamForBankTransferExec() with accountId:" + accountId, e);
    }
    
     return params;
  }
  
  public IGeneralDBManager getDbGeneralManager()
  {
     return dbGeneralManager;
  }
  
  public void setDbGeneralManager(IGeneralDBManager dbGeneralManager)
  {
     this.dbGeneralManager = dbGeneralManager;
  }
  



  public List<NameValuePair> buildParamForUpdateUserProfile(String mid, String accountId, String name, String email, String dob, String signature)
  {
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NAME, name));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_EMAIL, email));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_DOB, dob));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, signature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.CHANGEDATAUSER.toString()));

    }
    catch (Exception e)
    {
       logger.error("buildParamForUpdateUserProfile() with accountId:" + accountId, e);
    }
    
     return params;
  }
  
  public List<NameValuePair> buildNotifyBalance(String mid, String accountId, Long balance)
  {
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountId));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_BALANCE, String.valueOf(balance)));
    }
    catch (Exception e)
    {
       logger.error("buildParamForUpdateUserProfile() with accountId:" + accountId, e);
    }
    
     return params;
  }
  

  public List<NameValuePair> buildParamForInqTopUpSelf(String mid, String accountid, String nohpdest, Long amount, String networkSignature)
  {
     List<NameValuePair> params = new ArrayList();
    try {
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountid));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHPDEST, nohpdest));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, networkSignature));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TOPUPOTHERINQ.toString()));
       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
    } catch (Exception e) {
       logger.error("buildParamForInqTopUpSelf() with accountId:" + accountid, e);
    }
    
     return params;
  }

@Override
public List<NameValuePair> buildParamForExecTopUpSelf(String mid, String accountid, String nohpdest, Long amount,
		String networkSignature) {
	  List<NameValuePair> params = new ArrayList();
	    try {
	       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_MID, mid));
	       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_ACCOUNTID, accountid));
	       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_NOHPDEST, nohpdest));
	       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_PIN, Constant.DEFAULT_PIN));
	       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_SIGNATURE, networkSignature));
	       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_CALLTYPE, NetworkWorker.Operation.TOPUPOTHERTRANS.toString()));
	       params.add(new BasicNameValuePair(Constant.KEY_USER_INFO_AMOUNT, String.valueOf(amount)));
	    } catch (Exception e) {
	       logger.error("buildParamForExecTopUpSelf() with accountId:" + accountid, e);
	    }
	    
	     return params;
}
}