/*     */ package com.endpoint.alto.hibernate.services;
/*     */ 
/*     */ import com.endpoint.alto.hibernate.interfaces.IGeneralAction;
/*     */ import com.endpoint.alto.hibernate.interfaces.IGeneralDBManager;
/*     */ import com.endpoint.alto.hibernate.model.Account;
/*     */ import com.endpoint.alto.hibernate.model.Merchant;
/*     */ import com.endpoint.alto.hibernate.model.MerchantNetwork;
/*     */ import com.endpoint.alto.util.IdentityUtils;
			import com.endpoint.alto.util.NetworkWorker;
/*     */ import com.endpoint.alto.util.NetworkWorker.Operation;
/*     */ import java.io.PrintStream;
/*     */ import java.text.DateFormat;
/*     */ import java.text.ParseException;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ public class GeneralAction
/*     */   implements IGeneralAction
/*     */ {
/*     */   private IGeneralDBManager dbGeneralManager;
/*  22 */   private Logger logger = Logger.getLogger(GeneralAction.class);
/*  23 */   private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
/*     */   
/*     */ 
/*     */   public String buildNetworkSignature(String mid, String accountId, NetworkWorker.Operation calltype)
/*     */   {
/*  28 */     Merchant merchant = dbGeneralManager.getMerchantById(Long.valueOf(Long.parseLong(mid)));
/*     */     
/*  30 */     String keyAlto = merchant.getMerchantNetwork().getKey_network();
/*  31 */     String keyLvl1 = keyAlto + String.valueOf(merchant.getMerchantId());
/*     */     
/*  33 */     String hashSHA1 = IdentityUtils.createSignature("SHA-1", keyLvl1);
/*  34 */     String hashSHA256 = IdentityUtils.createSignature("SHA-256", calltype + hashSHA1 + accountId + "000000");
/*     */     
/*  36 */     logger.info("Build network signature by mid and account id:" + mid + ", " + accountId);
/*  37 */     return hashSHA256;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isSignatureValid(String mid, String accountId, String signature, NetworkWorker.Operation calltype)
/*     */   {
/*  66 */     String merchantSignature = buildMerchantSignature(mid, accountId, calltype);
/*  67 */     logger.info("Build merchant signature by mid and account id:" + mid + ", " + accountId);
/*  68 */     return signature.equals(merchantSignature);
/*     */   }
/*     */   
/*     */   public String buildMerchantSignature(String mid, String accountId, NetworkWorker.Operation calltype)
/*     */   {
/*  73 */     Merchant merchant = dbGeneralManager.getMerchantById(Long.valueOf(Long.parseLong(mid)));
/*  74 */     String keyMerchant = merchant.getKey_merchant();
/*  75 */     String keyLvl1 = keyMerchant + String.valueOf(merchant.getMerchantId());
/*     */     
/*  77 */     String hashSHA1 = IdentityUtils.createSignature("SHA-1", keyLvl1);
/*  78 */     String hashSHA256 = IdentityUtils.createSignature("SHA-256", calltype + hashSHA1 + accountId + "000000");
/*  79 */     return hashSHA256;
/*     */   }
/*     */   
/*     */   public boolean isSignatureValidForRegistration(String mid, String signature, String mobileNo)
/*     */   {
/*  84 */     Merchant merchant = dbGeneralManager.getMerchantById(Long.valueOf(Long.parseLong(mid)));
/*     */     
/*  86 */     String sMerchantId = String.valueOf(merchant.getMerchantId());
/*     */     
/*  88 */     String signatureLvl1 = IdentityUtils.createSignature("SHA-1", sMerchantId + mobileNo);
/*     */     
/*  90 */     String unsigned = signatureLvl1 + merchant.getKey_merchant();
/*  91 */     String signatureBuild = IdentityUtils.createSignature("SHA-256", unsigned);
/*     */     
/*  93 */     logger.info("MobileNo:" + mobileNo + ", mid:" + merchant.getMerchantId() + ", signature:" + signature);
/*  94 */     logger.info("Generated signature:" + signatureBuild);
/*     */     
/*  96 */     boolean result = signatureBuild.equals(signature);
/*  97 */     logger.info("Result of signature validation: " + result);
/*     */     
/*  99 */     return result;
/*     */   }
/*     */   
/*     */   public static void main(String[] args)
/*     */   {
/* 104 */     String signatureLvl1 = IdentityUtils.createSignature("SHA-1", "2200160001578");
/*     */     
/* 106 */     String unsignedSignature = signatureLvl1 + "1111TEST1234567890";
/* 107 */     String merchantSignature = IdentityUtils.createSignature("SHA-256", unsignedSignature);
/* 108 */     System.out.println(merchantSignature);
/*     */   }
/*     */   
/*     */   public String buildNetworkSignatureForRegistration(String mid, String mobileNo, NetworkWorker.Operation registration)
/*     */   {
/* 113 */     Merchant merchant = dbGeneralManager.getMerchantById(Long.valueOf(Long.parseLong(mid)));
/*     */     
/* 115 */     String keyAlto = merchant.getMerchantNetwork().getKey_network();
/* 116 */     String keyLvl1 = keyAlto + String.valueOf(merchant.getMerchantId());
/*     */     
/* 118 */     String hashSHA1 = IdentityUtils.createSignature("SHA-1", keyLvl1);
/* 119 */     String hashSHA256 = IdentityUtils.createSignature("SHA-256", registration + hashSHA1 + mobileNo + "000000");
/*     */     
/* 121 */     logger.info("Mobile no:" + mobileNo + ", mid:" + mid + ". Generated signature:" + hashSHA256);
/* 122 */     logger.info("Build network signature registration by mid:" + mid);
/* 123 */     return hashSHA256;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public boolean isMerchantExist(Long merchantId)
/*     */   {
/* 130 */     Merchant m = dbGeneralManager.getMerchantById(merchantId);
/*     */     
/* 132 */     return m != null;
/*     */   }
/*     */   
/*     */   public void saveAccount(String mid, Long accountId, String mobileNo, String email, String name, String dateOfBirth)
/*     */   {
/* 137 */     Merchant merchant = dbGeneralManager.getMerchantById(Long.valueOf(Long.parseLong(mid)));
/* 138 */     Account account = new Account();
/* 139 */     account.setMobileNo(mobileNo);
/* 140 */     account.setAccountId(accountId);
/* 141 */     account.setMerchant(merchant);
/* 142 */     account.setType(Account.ACCOUNT_TYPE_UNREGISTERD);
/* 143 */     account.setIsActive(Boolean.valueOf(true));
/* 144 */     account.setFullname(name);
/* 145 */     account.setEmail(email);
/*     */     try {
/* 147 */       Date formattedDateOfBirth = dateFormat.parse(dateOfBirth);
/* 148 */       account.setDateOfBirth(formattedDateOfBirth);
/*     */     } catch (ParseException e) {
/* 150 */       logger.error("saveAccount", e);
/*     */     }
/*     */     
/*     */ 
/* 154 */     dbGeneralManager.saveAccount(account);
/*     */   }
/*     */   
/*     */ 
/*     */   public Integer getAccountStatus(Long accountId)
/*     */   {
/* 160 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 161 */     if (account == null) {
/* 162 */       return Integer.valueOf(-1);
/*     */     }
/* 164 */     if (!account.getIsActive().booleanValue()) {
/* 165 */       return Integer.valueOf(1);
/*     */     }
/* 167 */     return Integer.valueOf(0);
/*     */   }
/*     */   
/*     */ 
/*     */   public void setUserAsRegister(Long accountId)
/*     */   {
/* 173 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 174 */     account.setType(Account.ACCOUNT_TYPE_REGISTERED);
/* 175 */     dbGeneralManager.saveAccount(account);
/*     */   }
/*     */   
/*     */   public void deactivateAccount(Long accountId)
/*     */   {
/* 180 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 181 */     account.setIsActive(Boolean.valueOf(false));
/* 182 */     dbGeneralManager.saveAccount(account);
/*     */   }
/*     */   
/*     */   public Short getAccountType(Long accountId) {
/* 186 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 187 */     return account.getType();
/*     */   }
/*     */   
/*     */   public void saveNewPhoneNumber(Long accountId, String newMobileNo)
/*     */   {
/* 192 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 193 */     account.setMobileNo(newMobileNo);
/* 194 */     dbGeneralManager.saveAccount(account);
/*     */   }
/*     */   
/*     */   public String getMobileNoByAccountId(Long accountId)
/*     */   {
/* 199 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 200 */     return account.getMobileNo();
/*     */   }
/*     */   
/*     */   public void setUserAsMerchant(Long accountId)
/*     */   {
/* 205 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 206 */     account.setType(Account.ACCOUNT_TYPE_MERCHANT);
/* 207 */     dbGeneralManager.saveAccount(account);
/*     */   }
/*     */   
/*     */ 
/*     */   public String generateInternalSignature(String mid, String accountId, NetworkWorker.Operation calltype)
/*     */   {
/* 213 */     Merchant merchant = dbGeneralManager.getMerchantById(Long.valueOf(Long.parseLong(mid)));
/*     */     
/* 215 */     String keyAlto = merchant.getKey_merchant();
/* 216 */     String keyLvl1 = keyAlto + String.valueOf(merchant.getMerchantId());
/*     */     
/* 218 */     String hashSHA1 = IdentityUtils.createSignature("SHA-1", keyLvl1);
/* 219 */     String hashSHA256 = IdentityUtils.createSignature("SHA-256", calltype + hashSHA1 + accountId + "000000");
/*     */     
/* 221 */     logger.info("Build network signature by mid and account id:" + mid + ", " + accountId);
/* 222 */     return hashSHA256;
/*     */   }
/*     */   
/*     */   public boolean isAccountMerchant(String accountId) {
/* 226 */     Account account = dbGeneralManager.getAccoutByAccountId(Long.valueOf(Long.parseLong(accountId)));
/* 227 */     if (account.getType() == Account.ACCOUNT_TYPE_MERCHANT) {
/* 228 */       return true;
/*     */     }
/* 230 */     return false;
/*     */   }
/*     */   
/*     */   public Long getAlreadyRegisteredAccountIdByMobileNo(String mobileNo)
/*     */   {
/* 235 */     Account account = dbGeneralManager.getAccoutByMobileNo(mobileNo);
/* 236 */     if (account != null) {
/* 237 */       return account.getAccountId();
/*     */     }
/* 239 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */   public void saveUpdateAccount(Long accountId, String name, String email, String dob)
/*     */   {
/* 245 */     Account account = dbGeneralManager.getAccoutByAccountId(accountId);
/* 246 */     account.setFullname(name);
/* 247 */     account.setEmail(email);
/*     */     try
/*     */     {
/* 250 */       Date dateOfBirth = dateFormat.parse(dob);
/* 251 */       account.setDateOfBirth(dateOfBirth);
/* 252 */       dbGeneralManager.saveAccount(account);
/*     */     } catch (ParseException e) {
/* 254 */       logger.error("saveUpdateAccount", e);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public IGeneralDBManager getDbGeneralManager()
/*     */   {
/* 261 */     return dbGeneralManager;
/*     */   }
/*     */   
/*     */   public void setDbGeneralManager(IGeneralDBManager dbGeneralManager)
/*     */   {
/* 266 */     this.dbGeneralManager = dbGeneralManager;
/*     */   }
/*     */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.services.GeneralAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */