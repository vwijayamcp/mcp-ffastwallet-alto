/*    */ package com.endpoint.alto.hibernate.services;
/*    */ 
/*    */ import com.endpoint.alto.hibernate.interfaces.ILogRequestResponseManager;
/*    */ import com.endpoint.alto.hibernate.interfaces.ILogRequestResponseService;
/*    */ import com.endpoint.alto.hibernate.model.RequestResponseLog;
/*    */ import java.util.Calendar;
/*    */ import java.util.Date;
/*    */ import java.util.List;
/*    */ import java.util.Map;
/*    */ import org.apache.http.NameValuePair;
/*    */ import org.json.JSONObject;
/*    */ 
/*    */ 
/*    */ public class CLogRequestResponseService
/*    */   implements ILogRequestResponseService
/*    */ {
/*    */   private ILogRequestResponseManager logManager;
/*    */   
/*    */   public Long saveRequestLog(Long processId, List<NameValuePair> params)
/*    */   {
/* 21 */     String requestContent = convertIntoPlainString(params);
/* 22 */     RequestResponseLog log = new RequestResponseLog();
/* 23 */     Date requestTime = getTimeStamp();
/* 24 */     log.setLogId(processId);
/* 25 */     log.setRequestContent(requestContent);
/* 26 */     log.setRequestIn(requestTime);
/* 27 */     logManager.saveRequestLog(log);
/* 28 */     return processId;
/*    */   }
/*    */   
/*    */   private String convertIntoPlainString(List<NameValuePair> params) {
/* 32 */     String result = "";
/* 33 */     for (NameValuePair param : params) {
/* 34 */       String paramName = param.getName();
/* 35 */       String value = param.getValue();
/* 36 */       result = result + paramName + "=" + value + "&";
/*    */     }
/*    */     
/* 39 */     if (!result.isEmpty()) {
/* 40 */       result = result.substring(0, result.length() - 1);
/*    */     }
/* 42 */     return result;
/*    */   }
/*    */   
/*    */   public void saveResponseLog(Long processId, String responseContent) {
/* 46 */     RequestResponseLog log = logManager.getRequestLogById(processId);
/* 47 */     if (log == null) return;
/* 48 */     Date responseTime = getTimeStamp();
/*    */     
/* 50 */     log.setResponseContent(responseContent);
/* 51 */     log.setResponseOut(responseTime);
/* 52 */     logManager.saveResponseLog(log);
/*    */   }
/*    */   
/*    */   public Long saveRequestLog(Long processId, String params) {
/* 56 */     RequestResponseLog log = new RequestResponseLog();
/* 57 */     Date requestTime = getTimeStamp();
/* 58 */     log.setLogId(processId);
/* 59 */     log.setRequestContent(params);
/* 60 */     log.setRequestIn(requestTime);
/* 61 */     Integer result = logManager.saveRequestLog(log);
/* 62 */     return new Long(result.intValue());
/*    */   }
/*    */   
/*    */   public void saveResponseLog(Long processId, Map result)
/*    */   {
/* 67 */     RequestResponseLog log = logManager.getRequestLogById(processId);
/* 68 */     if (log == null) return;
/* 69 */     JSONObject jsonObj = new JSONObject(result);
/* 70 */     Date responseTime = getTimeStamp();
/* 71 */     String responseContent = null;
/* 72 */     if (log.getResponseContent() != null) {
/* 73 */       responseContent = log.getResponseContent() + "-" + jsonObj.toString();
/*    */     } else {
/* 75 */       responseContent = jsonObj.toString();
/*    */     }
/* 77 */     log.setResponseContent(responseContent);
/* 78 */     log.setResponseOut(responseTime);
/* 79 */     logManager.saveResponseLog(log);
/*    */   }
/*    */   
/*    */   private Date getTimeStamp() {
/* 83 */     return Calendar.getInstance().getTime();
/*    */   }
/*    */   
/*    */   public ILogRequestResponseManager getLogManager() {
/* 87 */     return logManager;
/*    */   }
/*    */   
/*    */   public void setLogManager(ILogRequestResponseManager logManager) {
/* 91 */     this.logManager = logManager;
/*    */   }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.services.CLogRequestResponseService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */