package com.endpoint.alto.hibernate.interfaces;

import java.util.Map;

public abstract interface IStatusCodeAction
{
  public abstract String getTranslationStatusCode(Map paramMap);
}

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.interfaces.IStatusCodeAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */