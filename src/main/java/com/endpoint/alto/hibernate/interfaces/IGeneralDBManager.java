package com.endpoint.alto.hibernate.interfaces;

import com.endpoint.alto.hibernate.model.Account;
import com.endpoint.alto.hibernate.model.Merchant;
import com.endpoint.alto.hibernate.model.MerchantNetwork;
import com.endpoint.alto.hibernate.model.NetworkBank;
import java.util.List;

public abstract interface IGeneralDBManager
{
  public abstract int saveMerchant(Merchant paramMerchant);
  
  public abstract Merchant getMerchantById(Long paramLong);
  
  public abstract int deleteMerchant(Merchant paramMerchant);
  
  public abstract int saveNetwork(NetworkBank paramNetworkBank);
  
  public abstract NetworkBank getNetworkBankById(Long paramLong);
  
  public abstract int deleteNetworkBank(NetworkBank paramNetworkBank);
  
  public abstract int saveAccount(Account paramAccount);
  
  public abstract Account getAccoutByAccountId(Long paramLong);
  
  public abstract List<Account> getAllAccountsFromMerchant(Merchant paramMerchant);
  
  public abstract int deleteAccoutById(Long paramLong);
  
  public abstract int saveMerchantNetwork(MerchantNetwork paramMerchantNetwork);
  
  public abstract List<MerchantNetwork> getAllNetworkByMerchant(Merchant paramMerchant);
  
  public abstract List<MerchantNetwork> getAllMerchantByNetwork(NetworkBank paramNetworkBank);
  
  public abstract int deleteMerchantNetwork(MerchantNetwork paramMerchantNetwork);
  
  public abstract Account getAccoutByMobileNo(String paramString);
}

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.interfaces.IGeneralDBManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */