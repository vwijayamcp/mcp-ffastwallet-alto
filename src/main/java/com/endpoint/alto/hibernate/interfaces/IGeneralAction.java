package com.endpoint.alto.hibernate.interfaces;

import com.endpoint.alto.util.NetworkWorker;
import com.endpoint.alto.util.NetworkWorker.Operation;

public abstract interface IGeneralAction
{
  public abstract boolean isSignatureValid(String paramString1, String paramString2, String paramString3, NetworkWorker.Operation paramOperation);
  
  public abstract String buildNetworkSignature(String paramString1, String paramString2, NetworkWorker.Operation paramOperation);
  
  public abstract String buildNetworkSignatureForRegistration(String paramString1, String paramString2, NetworkWorker.Operation paramOperation);
  
  public abstract boolean isMerchantExist(Long paramLong);
  
  public abstract void saveAccount(String paramString1, Long paramLong, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract Integer getAccountStatus(Long paramLong);
  
  public abstract void deactivateAccount(Long paramLong);
  
  public abstract void setUserAsRegister(Long paramLong);
  
  public abstract void saveNewPhoneNumber(Long paramLong, String paramString);
  
  public abstract Short getAccountType(Long paramLong);
  
  public abstract String getMobileNoByAccountId(Long paramLong);
  
  public abstract void setUserAsMerchant(Long paramLong);
  
  public abstract boolean isAccountMerchant(String paramString);
  
  public abstract String generateInternalSignature(String paramString1, String paramString2, NetworkWorker.Operation paramOperation);
  
  public abstract Long getAlreadyRegisteredAccountIdByMobileNo(String paramString);
  
  public abstract void saveUpdateAccount(Long paramLong, String paramString1, String paramString2, String paramString3);
  
  public abstract String buildMerchantSignature(String paramString1, String paramString2, NetworkWorker.Operation paramOperation);
}

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.interfaces.IGeneralAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */