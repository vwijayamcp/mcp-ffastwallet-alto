package com.endpoint.alto.hibernate.interfaces;

import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;

public abstract interface ILogRequestResponseService
{
  public abstract Long saveRequestLog(Long paramLong, List<NameValuePair> paramList);
  
  public abstract Long saveRequestLog(Long paramLong, String paramString);
  
  public abstract void saveResponseLog(Long paramLong, String paramString);
  
  public abstract void saveResponseLog(Long paramLong, Map paramMap);
}

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.interfaces.ILogRequestResponseService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */