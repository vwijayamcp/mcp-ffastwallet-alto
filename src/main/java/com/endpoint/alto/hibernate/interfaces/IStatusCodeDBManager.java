package com.endpoint.alto.hibernate.interfaces;

import com.endpoint.alto.hibernate.model.StatusCode;

public abstract interface IStatusCodeDBManager
{
  public abstract StatusCode getStatusCodeByAltoCode(Long paramLong);
  
  public abstract StatusCode getStatusCodeByErrorCode(Long paramLong);
}

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.interfaces.IStatusCodeDBManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */