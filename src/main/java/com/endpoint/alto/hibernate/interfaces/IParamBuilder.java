package com.endpoint.alto.hibernate.interfaces;

import java.util.List;
import org.apache.http.NameValuePair;

public abstract interface IParamBuilder
{
  public abstract List<NameValuePair> buildParamForCheckBalance(String paramString1, String paramString2, String paramString3);
  
  public abstract List<NameValuePair> buildParamForLogin(String paramString1, String paramString2, String paramString3);
  
  public abstract List<NameValuePair> buildParamForRegistration(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6);
  
  public abstract List<NameValuePair> buildParamForCloseAccount(String paramString1, String paramString2, String paramString3);
  
  public abstract List<NameValuePair> buildParamForChangeMobileNoInq(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForChangeMobileNoExecute(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForUpgradeUserInq(String paramString1, String paramString2, String paramString3);
  
  public abstract List<NameValuePair> buildParamForUpgradeUserExec(String paramString1, String paramString2, String paramString3);
  
  public abstract List<NameValuePair> buildParamForP2PInqPayment(String paramString1, String paramString2, String paramString3, Long paramLong, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForP2PExecutePayment(String paramString1, String paramString2, String paramString3, Long paramLong, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForGetTransaction(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForInqMultiChanelTransfer(String paramString1, String paramString2, String paramString3, Long paramLong, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForExecMultiChanelTransfer(String paramString1, String paramString2, String paramString3, Long paramLong, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForUpgradeUserMerchantInq(String paramString1, String paramString2, String paramString3);
  
  public abstract List<NameValuePair> buildParamForUpgradeUserMerchantExec(String paramString1, String paramString2, String paramString3);
  
  public abstract List<NameValuePair> buildParamForBankTransferInq(String paramString1, String paramString2, Long paramLong, String paramString3, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForBankTransferExec(String paramString1, String paramString2, Long paramLong, String paramString3, String paramString4, String paramString5);
  
  public abstract List<NameValuePair> buildParamForUpdateUserProfile(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6);
  
  public abstract List<NameValuePair> buildNotifyBalance(String paramString1, String paramString2, Long paramLong);
  
  public abstract List<NameValuePair> buildParamForInqTopUpSelf(String paramString1, String paramString2, String paramString3, Long paramLong, String paramString4);

public abstract List<NameValuePair> buildParamForExecTopUpSelf(String mid, String accountid, String nohpdest,
		Long amount, String networkSignature);
}

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.interfaces.IParamBuilder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */