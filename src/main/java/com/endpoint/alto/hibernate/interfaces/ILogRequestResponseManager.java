package com.endpoint.alto.hibernate.interfaces;

import com.endpoint.alto.hibernate.model.RequestResponseLog;

public abstract interface ILogRequestResponseManager
{
  public abstract Integer saveRequestLog(RequestResponseLog paramRequestResponseLog);
  
  public abstract RequestResponseLog getRequestLogById(Long paramLong);
  
  public abstract Integer saveResponseLog(RequestResponseLog paramRequestResponseLog);
}

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.hibernate.interfaces.ILogRequestResponseManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */