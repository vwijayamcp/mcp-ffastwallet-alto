 package com.endpoint.alto.util;
 
 import com.endpoint.alto.hibernate.interfaces.ILogRequestResponseService;
import com.endpoint.central.service.util.Constant;
import com.endpoint.central.service.util.Status;

 import java.io.BufferedReader;
 import java.io.IOException;
 import java.io.InputStream;
 import java.io.InputStreamReader;
 import java.security.MessageDigest;
 import java.util.ArrayList;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 import java.util.Properties;
 import org.apache.http.HttpEntity;
 import org.apache.http.HttpResponse;
 import org.apache.http.NameValuePair;
 import org.apache.http.client.HttpClient;
 import org.apache.http.client.entity.UrlEncodedFormEntity;
 import org.apache.http.client.methods.HttpPost;
 import org.apache.http.impl.client.DefaultHttpClient;
 import org.apache.log4j.Logger;
 import org.json.JSONArray;
 import org.json.JSONObject;
 
 
 public class NetworkWorker
 {
   private static Logger logger = Logger.getLogger(NetworkWorker.class);
   private ILogRequestResponseService requestResponeLogService;
   private static final String USER_AGENT = "Mozilla/5.0";
   
   public static enum Operation { REGISTRATION,  SALDO,  LOGIN,  CLOSEACCOUNT,  CHANGENOHP,  CHANGEPRODUCT,  TRANSFERINQ,  TRANSFERTRANS, 
     LASTTRANS,  TRANSFERMERCHANTINQ,  TRANSFERMERCHANTTRANS,  CHANGEPRODUCTMERCHANT,  TRANSFERBANKINQ, 
     TRANSFERBANKTRANS,  CHANGEDATAUSER,  TOPUPOTHERINQ,  TOPUPOTHERTRANS;
     
     }
   
   public Map sendRequest(Long processId, List<NameValuePair> params, String url) { Map m = new HashMap();
				JSONObject json = null;
     try {
       json = execute(url, new DefaultHttpClient(), new HttpPost(url), params);
       
       for (String key : json.keySet())
       {
         Object value = json.get(key);
         if ((value instanceof JSONArray)) {
           JSONArray resultArray = (JSONArray)value;
           List<Map> list = new ArrayList();
           JSONObject indexValue; for (int i = 0; i < resultArray.length(); i++) {
             indexValue = resultArray.getJSONObject(i);
             for (String singleResultKey : indexValue.keySet()) {
               Map singleResultDictionary = new HashMap();
               singleResultDictionary.put(singleResultKey, indexValue.get(singleResultKey));
               list.add(singleResultDictionary);
             }
           }
           value = list;
         }
         m.put(key, value);
       }
     } catch (Exception e) { 
       requestResponeLogService.saveResponseLog(processId, e.getLocalizedMessage());
       m.put(Constant.KEY_RESULT_CODE, Status.ALTO_INTERNAL_ERROR);
     }
     m.put(Constant.PROCESS_ID, String.valueOf(processId));
     m.remove(Status.KEY_RESULT_MESSAGE);
     return m;
   }
   
   private JSONObject execute(String url, HttpClient client, HttpPost post, List<NameValuePair> params) throws Exception
   {
     post.setHeader("User-Agent", "Mozilla/5.0");
     if (params != null) {
       post.setEntity(new UrlEncodedFormEntity(params));
     }
     long start_time = System.nanoTime();
     HttpResponse response = client.execute(post);
     long end_time = System.nanoTime();
     double difference = (end_time - start_time) / 1000000.0D;
     
 
     BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
     
     StringBuffer result = new StringBuffer();
     String line = "";
     while ((line = rd.readLine()) != null) {
       result.append(line);
     }
     
     JSONObject jsonResult = null;
     try {
       jsonResult = new JSONObject(result.toString());
       logger.info("result:" + jsonResult);
     } catch (Exception e) {
       Map errorResult = new HashMap();
       errorResult.put(Constant.KEY_RESULT_CODE, Status.NO_RESPONSE_FROM_HOST);
       jsonResult = new JSONObject(errorResult);
       logger.error("execute", e);
     }
     return jsonResult;
   }
   
   private String createSignature(String alg, String value)
   {
     try {
       MessageDigest md = MessageDigest.getInstance(alg);
       md.update(value.getBytes());
       byte[] sha256 = md.digest();
       return byteArrayToHex(sha256);
     } catch (Exception e) {}
     return "error";
   }
   
   private static String byteArrayToHex(byte[] a)
   {
     StringBuilder sb = new StringBuilder();
     for (byte b : a)
       sb.append(String.format("%02x", new Object[] { Integer.valueOf(b & 0xFF) }));
     return sb.toString();
   }
   
   public static String getEndpointURLReg() {
     return getAppConfig("app.url.reg");
   }
   
   public static String getEndpointURLService() {
     return getAppConfig("app.url.services");
   }
   
   public static String getEndpointURLData() {
     return getAppConfig("app.url.data");
   }
   
   public static String getEndPointNotifyBalance() {
     return getAppConfig("app.url.notify.balane");
   }
   
 
   private static String getAppConfig(String key)
   {
     Properties prop = new Properties();
     InputStream input = null;
     String result = null;
     try
     {
       ClassLoader loader = Thread.currentThread().getContextClassLoader();
       input = loader.getResourceAsStream("endpoint.properties");
       prop.load(input);
       
       return prop.getProperty(key);
     }
     catch (Exception ex)
     {
       ex.printStackTrace();
       
     }
     finally
     {
       if (input != null)
         try {
           input.close();
         } catch (IOException e) {
           e.printStackTrace();
         }
     }
     return result;
   }
   
   public ILogRequestResponseService getRequestResponeLogService()
   {
     return requestResponeLogService;
   }
   
   public void setRequestResponeLogService(ILogRequestResponseService requestResponeLogService) {
     this.requestResponeLogService = requestResponeLogService;
   }
 }
