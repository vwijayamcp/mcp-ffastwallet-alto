/*    */ package com.endpoint.alto.util;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import java.io.InputStream;
/*    */ import java.security.MessageDigest;
/*    */ import java.security.NoSuchAlgorithmException;
/*    */ import java.util.Properties;
/*    */ 
/*    */ public class IdentityUtils
/*    */ {
/*    */   public static final String HASH_ALG_SHA1 = "SHA-1";
/*    */   public static final String HASH_ALG_SHA256 = "SHA-256";
/*    */   
/*    */   public static String constructHash(String accountId, String key)
/*    */   {
/* 16 */     String result = "";
/* 17 */     String input = accountId + key;
/*    */     try
/*    */     {
/* 20 */       MessageDigest md = MessageDigest.getInstance("SHA-256");
/* 21 */       md.update(input.getBytes());
/* 22 */       byte[] sha256 = md.digest();
/* 23 */       StringBuilder sb = new StringBuilder();
/* 24 */       for (byte b : sha256) {
/* 25 */         sb.append(String.format("%02x", new Object[] { Integer.valueOf(b & 0xFF) }));
/*    */       }
/* 27 */       result = sb.toString();
/*    */     } catch (NoSuchAlgorithmException e) {
/* 29 */       e.printStackTrace();
/*    */     }
/*    */     
/* 32 */     return result;
/*    */   }
/*    */   
/*    */   public static String getAppLanguage()
/*    */   {
/* 37 */     Properties prop = new Properties();
/* 38 */     InputStream input = null;
/* 39 */     String result = null;
/*    */     try
/*    */     {
/* 42 */       ClassLoader loader = Thread.currentThread().getContextClassLoader();
/* 43 */       input = loader.getResourceAsStream("endpoint.properties");
/* 44 */       prop.load(input);
/*    */       
/* 46 */       return prop.getProperty("app.language");
/*    */     }
/*    */     catch (IOException ex)
/*    */     {
/* 50 */       ex.printStackTrace();
/*    */       
/* 59 */       return result;
/*    */     }
/*    */     finally
/*    */     {
/* 52 */       if (input != null)
/*    */         try {
/* 54 */           input.close();
/*    */         } catch (IOException e) {
/* 56 */           e.printStackTrace();
/*    */         }
/*    */     }
/* 59 */     
/*    */   }
/*    */   
/*    */   public static String createSignature(String alg, String value)
/*    */   {
/*    */     try
/*    */     {
/* 66 */       MessageDigest md = MessageDigest.getInstance(alg);
/* 67 */       md.update(value.getBytes());
/* 68 */       byte[] sha256 = md.digest();
/* 69 */       return byteArrayToHex(sha256);
/*    */     } catch (Exception e) {}
/* 71 */     return "-1";
/*    */   }
/*    */   
/*    */   private static String byteArrayToHex(byte[] a)
/*    */   {
/* 76 */     StringBuilder sb = new StringBuilder();
/* 77 */     for (byte b : a)
/* 78 */       sb.append(String.format("%02x", new Object[] { Integer.valueOf(b & 0xFF) }));
/* 79 */     return sb.toString();
/*    */   }
/*    */ }

/* Location:           C:\Users\gregory.laksono\Desktop\ALTO-0.0.1-SNAPSHOT.jar
 * Qualified Name:     com.endpoint.alto.util.IdentityUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */