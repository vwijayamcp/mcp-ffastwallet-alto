package my.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.endpoint.alto.hibernate.model.Account;
import com.endpoint.alto.util.NetworkWorker;
import com.endpoint.central.service.util.Constant;

public class ParamBuilder {
	private static  String BANKID = "bankid";
	private static  String NOREK = "norek";
	private static String MID = "mid";
	private static String ACCOUNT_ID = "accountid";
	private static String NOHP = "nohp";
	private static String NOHPDEST = "nohpdest";
	private static String NAME = "name";
	private static String EMAIL = "email";
	private static String DOB = "dob";
	private static String SIGNATURE = "signature";
	private static String ACCOUNT_ID_DEST = "accountiddest";
	private static String AMOUNT = "amount";
	
	
	
	 public List<NameValuePair> buildParamForRegister(String mid, String nohp, String name, String dob, String email, String signature)
	  {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(NOHP, nohp));
	     params.add(new BasicNameValuePair(NAME, name));
	     params.add(new BasicNameValuePair(DOB, dob));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));
	     params.add(new BasicNameValuePair(EMAIL, email));

	     return params;
	  }


	public List<NameValuePair> buildParamInqUpgradeRegister(String mid, String accountid, String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}


	public List<NameValuePair> buildParamExecUpgradeRegister(String mid, String accountid, String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}


	public List<NameValuePair> buildParamInqP2PTrx(String mid, String accountid1, String accountid2, Long amount,
			String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid1));
	     params.add(new BasicNameValuePair(ACCOUNT_ID_DEST, accountid2));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}
	
	public List<NameValuePair> buildParamExecP2PTrx(String mid, String accountid1, String accountid2, Long amount,
			String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid1));
	     params.add(new BasicNameValuePair(ACCOUNT_ID_DEST, accountid2));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}
	
	public List<NameValuePair> buildParamInqUpgradeMerchant(String mid, String accountid, String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}
	
	public List<NameValuePair> buildParamExecUpgradeMerchant(String mid, String accountid, String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}


	public List<NameValuePair> buildParamInqP2M(String mid, String accountid1, Long amount, String accountid2,
			String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid1));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(ACCOUNT_ID_DEST, accountid2));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}


	public List<NameValuePair> buildParamExecP2M(String mid, String accountid1, Long amount, String accountid2,
			String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid1));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(ACCOUNT_ID_DEST, accountid2));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));

	     return params;
	}


	public List<NameValuePair> buildParamCheckBalance(String mid, String accountid, String signature) {
	     List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));
		return params;
	}


	public List<NameValuePair> buildParamInqTopUp(String mid, String accountid, String nohp, Long amount,
			String signature) {
		 List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(NOHPDEST, nohp));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));
		return params;
	}


	public List<NameValuePair> buildParamExecTopUp(String mid, String accountid, String nohp, Long amount,
			String signature) {
		 List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(NOHPDEST, nohp));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));
		return params;
	}


	public List<NameValuePair> buildParamInqBankTransfer(String mid, String accountid, String bankid, Long amount,
			String norek, String signature) {

		 List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(BANKID, bankid));
	     params.add(new BasicNameValuePair(NOREK, norek));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));
		return params;
	}


	public List<NameValuePair> buildParamExecBankTransfer(String mid, String accountid, String bankid, Long amount,
			String norek, String signature) {
		 List<NameValuePair> params = new ArrayList();
	     params.add(new BasicNameValuePair(MID, mid));
	     params.add(new BasicNameValuePair(ACCOUNT_ID, accountid));
	     params.add(new BasicNameValuePair(BANKID, bankid));
	     params.add(new BasicNameValuePair(NOREK, norek));
	     params.add(new BasicNameValuePair(AMOUNT, String.valueOf(amount)));
	     params.add(new BasicNameValuePair(SIGNATURE, signature));
		return params;
	}
}
