package my.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert; 

import com.endpoint.alto.hibernate.model.Merchant;
import com.endpoint.alto.util.IdentityUtils;
import com.endpoint.alto.util.NetworkWorker;
import com.endpoint.alto.util.NetworkWorker.Operation;
import com.endpoint.central.service.util.Constant;
import com.endpoint.central.service.util.Status;

import javafx.geometry.HPos;

public class Test {
	
	private static String url = "http://localhost:8080/Ffastwallet/v2/ffservice/";
//	private static String KEY = "1111TEST12345678902";
	//1111TEST1234567890
	private static String KEY = "aX9zGdZBHHLmFO4yj9sJ0joiyq4f82";
	
	//ALTO KEY DEV = 3K0134526787YSGYSGYT2Y86
	private static String mid = "2";
	ParamBuilder paramBuilder = new ParamBuilder();
	List<NameValuePair> params = null;
	Map result = null;
	String resultCode = null;
	
	String nohp1 = "081219170116";
	String name1 = "User-"+new SimpleDateFormat("ssmmHHddMMYY").format(new Date());
	String dob1 = "11/01/1990";
	String email1 ="email@test.com";
	String accountid1 = "200160002009";
	
	String nohp2 = "081219180116";
	String name2 = "User-"+new SimpleDateFormat("ssmmHHddMMYY").format(new Date());
	String dob2 = "11/01/1990";
	String email2 ="email@test.com";
	String accountid2 = "200160002010";
	String signature = null;
	String bankid = "013";
	String norek = "101010";
	
	@org.junit.Test
	public void test(){
		signature = buildMerchantSignature(Operation.TRANSFERBANKINQ, accountid1);
		params = paramBuilder.buildParamInqBankTransfer(mid, accountid1, bankid, new Long("20000"), norek, signature);
		result = sendRequest(getProcessId(), params, url+"inqBankTransferNow");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		signature = buildMerchantSignature(Operation.TRANSFERBANKTRANS, accountid1);
		params = paramBuilder.buildParamExecBankTransfer(mid, accountid1, bankid, new Long("20000"), norek, signature);
		result = sendRequest(getProcessId(), params, url+"execBankTransferNow");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);	
//		testRegistration(nohp1, name1, dob1, email1, nohp2, name2, dob2, email2);
//		testAll(accountid1, accountid2);
		

//		signature = buildMerchantSignature(Operation.CHANGEPRODUCTMERCHANT, accountid1);
//		params = paramBuilder.buildParamInqUpgradeMerchant(mid, accountid1, signature);
//		result = sendRequest(getProcessId(), params, url+"inqUserAsMerchant");
//		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
//		Assert.assertEquals(Status.SUCCESS, resultCode);
//		
//		signature = buildMerchantSignature(Operation.CHANGEPRODUCTMERCHANT, accountid1);
//		params = paramBuilder.buildParamExecUpgradeMerchant(mid, accountid1, signature);
//		result = sendRequest(getProcessId(), params, url+"execUserAsMerchant");
//		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
//		Assert.assertEquals(Status.SUCCESS, resultCode);

//		signature = buildMerchantSignature(Operation.SALDO, accountid1);
//		params = paramBuilder.buildParamCheckBalance(mid, accountid1, signature);
//		result = sendRequest(getProcessId(), params, url+"checkBalance");
//		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
//		Assert.assertEquals(Status.SUCCESS, resultCode);
		
//		params = paramBuilder.buildParamCheckBalance(mid, accountid1, signature);
//		result = sendRequest(getProcessId(), new ArrayList(), url+"getMasterData");
//		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
//		Assert.assertEquals(Status.SUCCESS, resultCode);
		
//		String sig = buildMerchantSignature(Operation.REGISTRATION, "081310005555");
//		Assert.assertEquals(sig, "3dbbd2e3d2ffd9d75a7b1e453a0225cc310e0d4ed374925500cc0d1b41b45512");;
		
	}
	
	private void testRegistration(String nohp1,String name1,String dob1,String email1,
								  String nohp2,String name2,String dob2,String email2){
	  	Long processId = getProcessId();
	  	String signature  = null;
		
		signature = buildMerchantSignature(Operation.REGISTRATION, nohp1);
		params = paramBuilder.buildParamForRegister(mid, nohp1, name1, dob1, email1, signature);
		result = sendRequest(processId, params, url+"registerNewCustomer");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
	
		signature = buildMerchantSignature(Operation.REGISTRATION, nohp2);
		params = paramBuilder.buildParamForRegister(mid, nohp2, name2, dob2, email2, signature);
		result = sendRequest(processId, params, url+"registerNewCustomer");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
	}

  private void testAll(String accountid1, String accountid2, String bankid, String norek) {
	  	Long processId = getProcessId();
	  	String signature  = null;
		Long amount = new Long(10000);

		//Test P2P transfer
	  	signature = buildMerchantSignature(Operation.TRANSFERINQ, accountid1);
		params = paramBuilder.buildParamInqP2PTrx(mid, accountid1, accountid2, amount, signature);
		result = sendRequest(processId, params, url+"inqP2PTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.REMMITER_ACCOUNT_TYPE_NOT_ALLOWED, resultCode);
		
	  	signature = buildMerchantSignature(Operation.TRANSFERTRANS, accountid1);
		params = paramBuilder.buildParamExecP2PTrx(mid, accountid1, accountid2, amount, signature);
		result = sendRequest(processId, params, url+"execP2PTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.REMMITER_ACCOUNT_TYPE_NOT_ALLOWED, resultCode);
		
	  	//Upgrade unregistered user 1 to user registered
		signature = buildMerchantSignature(Operation.CHANGEPRODUCT, accountid1);
		params = paramBuilder.buildParamInqUpgradeRegister(mid, accountid1, signature);
		result = sendRequest(processId, params, url+"inqUserAsRegistered");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		signature = buildMerchantSignature(Operation.CHANGEPRODUCT, accountid1);
		params = paramBuilder.buildParamExecUpgradeRegister(mid, accountid1, signature);
		result = sendRequest(processId, params, url+"execUserAsRegistered");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		//P2P transfer
	  	signature = buildMerchantSignature(Operation.TRANSFERINQ, accountid1);
		params = paramBuilder.buildParamInqP2PTrx(mid, accountid1, accountid2, amount, signature);
		result = sendRequest(processId, params, url+"inqP2PTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
	  	signature = buildMerchantSignature(Operation.TRANSFERTRANS, accountid1);
		params = paramBuilder.buildParamExecP2PTrx(mid, accountid1, accountid2, amount, signature);
		result = sendRequest(processId, params, url+"execP2PTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
	  	//Upgrade unregistered user 2 to user registered
		signature = buildMerchantSignature(Operation.CHANGEPRODUCT, accountid2);
		params = paramBuilder.buildParamInqUpgradeRegister(mid, accountid2, signature);
		result = sendRequest(processId, params, url+"inqUserAsRegistered");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		signature = buildMerchantSignature(Operation.CHANGEPRODUCT, accountid2);
		params = paramBuilder.buildParamExecUpgradeRegister(mid, accountid2, signature);
		result = sendRequest(processId, params, url+"execUserAsRegistered");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		//Upgrade user 1 as merchant
		signature = buildMerchantSignature(Operation.CHANGEPRODUCTMERCHANT, accountid1);
		params = paramBuilder.buildParamInqUpgradeMerchant(mid, accountid1, signature);
		result = sendRequest(processId, params, url+"inqUserAsMerchant");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		signature = buildMerchantSignature(Operation.CHANGEPRODUCTMERCHANT, accountid1);
		params = paramBuilder.buildParamExecUpgradeMerchant(mid, accountid1, signature);
		result = sendRequest(processId, params, url+"execUserAsMerchant");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		//Transfer user 1 as merchant to user 2 as registered 
		signature = buildMerchantSignature(Operation.TRANSFERMERCHANTINQ, accountid1);
		params = paramBuilder.buildParamInqP2M(mid, accountid1, amount, accountid2, signature);
		result = sendRequest(processId, params, url+"inqP2MTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.DESTACCOUNT_NOT_MERCHANT, resultCode);
		
		signature = buildMerchantSignature(Operation.TRANSFERMERCHANTTRANS, accountid1);
		params = paramBuilder.buildParamExecP2M(mid, accountid1, amount, accountid2, signature);
		result = sendRequest(processId, params, url+"execP2MTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.DESTACCOUNT_NOT_MERCHANT, resultCode);
		
		//Transfer user 2 as registered to user 1 as merchant
		signature = buildMerchantSignature(Operation.TRANSFERMERCHANTINQ, accountid2);
		params = paramBuilder.buildParamInqP2M(mid, accountid2, amount, accountid1, signature);
		result = sendRequest(processId, params, url+"inqP2MTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		signature = buildMerchantSignature(Operation.TRANSFERMERCHANTTRANS, accountid2);
		params = paramBuilder.buildParamExecP2M(mid, accountid2, amount, accountid1, signature);
		result = sendRequest(processId, params, url+"execP2MTransaction");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		//Topup self pulsa of account 1
		signature = buildMerchantSignature(Operation.TOPUPOTHERINQ, accountid1);
		params = paramBuilder.buildParamInqTopUp(mid, accountid1, nohp1, amount, signature);
		result = sendRequest(processId, params, url+"inqTopupPulsa");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		signature = buildMerchantSignature(Operation.TOPUPOTHERTRANS, accountid1);
		params = paramBuilder.buildParamExecTopUp(mid, accountid1, nohp1, amount, signature);
		result = sendRequest(processId, params, url+"execTopupPulsa");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		
		//Bank transfer from user 1 
		signature = buildMerchantSignature(Operation.TRANSFERBANKINQ, accountid1);
		params = paramBuilder.buildParamInqBankTransfer(mid, accountid1, bankid, amount, norek, signature);
		result = sendRequest(processId, params, url+"inqBankTransferNow");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
		
		signature = buildMerchantSignature(Operation.TOPUPOTHERTRANS, accountid1);
		params = paramBuilder.buildParamExecBankTransfer(mid, accountid1, bankid, amount, norek, signature);
		result = sendRequest(processId, params, url+"execBankTransferNow");
		resultCode = String.valueOf(result.get(Constant.KEY_RESULT_CODE));
		Assert.assertEquals(Status.SUCCESS, resultCode);
	}

public String buildMerchantSignature(NetworkWorker.Operation calltype, String accountId)
{

     String hashSHA1 = IdentityUtils.createSignature("SHA-1", KEY);
     String hashSHA256 = IdentityUtils.createSignature("SHA-256", calltype + hashSHA1 + accountId + "000000");
     return hashSHA256;
  }
  
  public Map sendRequest(Long processId, List<NameValuePair> params, String url) { Map m = new HashMap();
	JSONObject json = null;
		try {
			json = execute(url, new DefaultHttpClient(), new HttpPost(url), params);
			
			for (String key : json.keySet())
			{
				Object value = json.get(key);
				if ((value instanceof JSONArray)) {
					JSONArray resultArray = (JSONArray)value;
					List<Map> list = new ArrayList();
					JSONObject indexValue; for (int i = 0; i < resultArray.length(); i++) {
					indexValue = resultArray.getJSONObject(i);
					for (String singleResultKey : indexValue.keySet()) {
						 Map singleResultDictionary = new HashMap();
						 singleResultDictionary.put(singleResultKey, indexValue.get(singleResultKey));
						 list.add(singleResultDictionary);
						}
					}
						value = list;
					}
				m.put(key, value);
			}
		} catch (Exception e) { 

			m.put(Constant.KEY_RESULT_CODE, Status.ALTO_INTERNAL_ERROR);
		}
		m.put(Constant.PROCESS_ID, String.valueOf(processId));
		m.remove(Status.KEY_RESULT_MESSAGE);
		return m;
}

private JSONObject execute(String url, HttpClient client, HttpPost post, List<NameValuePair> params) throws Exception
{
post.setHeader("User-Agent", "Mozilla/5.0");
if (params != null) {
post.setEntity(new UrlEncodedFormEntity(params));
}
long start_time = System.nanoTime();
HttpResponse response = client.execute(post);
long end_time = System.nanoTime();
double difference = (end_time - start_time) / 1000000.0D;


BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

StringBuffer result = new StringBuffer();
String line = "";
while ((line = rd.readLine()) != null) {
result.append(line);
}

JSONObject jsonResult = null;
try {
jsonResult = new JSONObject(result.toString());

} catch (Exception e) {
Map errorResult = new HashMap();
errorResult.put(Constant.KEY_RESULT_CODE, Status.NO_RESPONSE_FROM_HOST);
jsonResult = new JSONObject(errorResult);

}
return jsonResult;
}
private Long getProcessId(){
	Date d = Calendar.getInstance().getTime();
	DateFormat format = new SimpleDateFormat("SSssmmHHDDMMyy");
	String formatedDate = format.format(d);
	Long processId = Long.parseLong(formatedDate);
	return processId;
}
}
