package com.endpoint.alto.test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class FWFrontend {

	String URL_FF = "/ffservice/";

	String ID_MERCHANT;	
	String KEY_FF;	
	String KEY_MERCHANT;
	
	Logger LOGGER;

	FWFrontend(String mid, String signatureKey, String host, Logger logger) {
		this.ID_MERCHANT = mid;
		this.KEY_FF = signatureKey;
		this.KEY_MERCHANT = FWUtil.createSignature(FWUtil.ALG_SHA1, KEY_FF + ID_MERCHANT);
		this.URL_FF = host + URL_FF;
		this.LOGGER = logger;
	}
	
	
	/****************************************************************************************************************/
	/* TEST STEP IMPLEMENTATION																						*/
	/****************************************************************************************************************/	

	protected void test_GetMasterData() {
		test_GetMasterData(null, null);
	}
	
	protected void test_GetMasterData(String checkParam, String checkValue) {
		
		try {
			FWUtil.log(LOGGER, "\nGET MASTER DATA");
			//String calltype = FWUtil.CALLTYPE_GETMASTERDATA;
			String URL_FF_case = "getMasterData";
			
			//String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			//List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null);
			List<NameValuePair> params = null;
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}
	
	protected String test_Registration(FWUser user) {
		return test_Registration(user, null, null);
	}
	
	protected String test_Registration(FWUser user, String checkParam, String checkValue) {
		
		String accountid = null;

		try {			
			FWUtil.log(LOGGER, "\nREGISTRATION");
			String calltype = FWUtil.CALLTYPE_REGISTRATION;
			String URL_FF_case = "registerNewCustomer";

			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.nohp + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
			accountid = FWUtil.getAccountIDInfo(response);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
		
		return accountid;
	}
	
	protected void test_Login(FWUser user) {
		test_Login(user, null, null);
	}
	
	protected void test_Login(FWUser user, String checkParam, String checkValue) {
		
		try {
			FWUtil.log(LOGGER, "\nLOGIN");
			String calltype = FWUtil.CALLTYPE_LOGIN;
			String URL_FF_case = "????"; //TODO

			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.nohp + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}
	
	protected void test_UpgradeUserToRegistered(FWUser user) {
		test_UpgradeUserToRegistered(user, null, null);
	}
	
	protected void test_UpgradeUserToRegistered(FWUser user, String checkParam, String checkValue) {
		
		try {
			String calltype = FWUtil.CALLTYPE_UPGRADETOREGISTEREDUSER;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params;
			String URL_FF_case;
			
			FWUtil.log(LOGGER, "\nCHANGE USER LEVEL UNREGISTERED TO REGISTERED (INQ)");
			URL_FF_case = "inqUserAsRegistered";
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "1", null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
			FWUtil.log(LOGGER, "\nCHANGE USER LEVEL UNREGISTERED TO REGISTERED (TRANS)");
			URL_FF_case = "execUserAsRegistered";
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "2", null, null, null, null, null, null, null, null, null);
			response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}

	protected void test_UpgradeUserToMerchant(FWUser user) {
		test_UpgradeUserToMerchant(user, null, null);
	}
	
	protected void test_UpgradeUserToMerchant(FWUser user, String checkParam, String checkValue) {
		
		try {
			String calltype = FWUtil.CALLTYPE_UPGRADETOMERCHANT;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params;
			String URL_FF_case;
			
			FWUtil.log(LOGGER, "\nCHANGE USER LEVEL UNREGISTERED TO MERCHANT (INQ)");
			URL_FF_case = "inqUserAsMerchant";
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "1", null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
			FWUtil.log(LOGGER, "\nCHANGE USER LEVEL UNREGISTERED TO MERCHANT (TRANS)");
			URL_FF_case = "execUserAsMerchant";
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "2", null, null, null, null, null, null, null, null, null);
			response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}	
	
	protected void test_CheckBalance(FWUser user) {
		test_CheckBalance(user, null, null);
	}
	
	protected void test_CheckBalance(FWUser user, String checkParam, String checkValue) {
		try {
			FWUtil.log(LOGGER, "\nCHECK BALANCE");
			String calltype = FWUtil.CALLTYPE_CHECKBALANCE;
			String URL_FF_case = "checkBalance";
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}

	protected void test_ChangePhoneNumber(FWUser user, String newphone) {
		test_ChangePhoneNumber(user, newphone, null, null);
	}
	
	protected void test_ChangePhoneNumber(FWUser user, String newphone, String checkParam, String checkValue) {
		try {
			String calltype = FWUtil.CALLTYPE_CHANGEPHONENUM;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params;
			String URL_FF_case;
			
			FWUtil.log(LOGGER, "\nCHANGE NO HP (INQ)");
			URL_FF_case = "inqChangePhoneNumber";	
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "1", newphone, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);		
			
			FWUtil.log(LOGGER, "\nCHANGE NO HP (TRANS)");
			URL_FF_case = "execChangePhoneNumber";
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "2", newphone, null, null, null, null, null, null, null, null);
			response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}
/*	
 	protected void test_ChangePIN(FWUser user, String newpin) {
 		test_ChangePIN(user, newpin, null, null);
 	}
 	
	protected void test_ChangePIN(FWUser user, String newpin, String checkParam, String checkValue) {
		try {		
			FWUtil.log(LOGGER, "\nCHANGE PIN");
			String calltype = FWUtil.CALLTYPE_CHANGEPIN;
			String signature = createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, newpin, newpin, null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			FWUtil.validateResponse(response, checkParam, checkValue, logger);		
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}
*/
	protected void test_CloseAccount(FWUser user) {
		test_CloseAccount(user, null, null);
	}
	
	protected void test_CloseAccount(FWUser user, String checkParam, String checkValue) {
		try {		
			FWUtil.log(LOGGER, "\nCLOSE ACCOUNT");
			String calltype = FWUtil.CALLTYPE_CLOSEACCOUNT;
			String URL_FF_case = "closeAccount";
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);

		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}
	
	protected void test_TransferP2P(FWUser sender, FWUser recipient, String amount) {
		test_TransferP2P(sender, recipient, amount, null, null);
	}
	
	protected void test_TransferP2P(FWUser sender, FWUser recipient, String amount, String checkParam, String checkValue) {
		try {
			String calltype;
			String signature;
			List<NameValuePair> params;
			String URL_FF_case;
			
			FWUtil.log(LOGGER, "\nTRANSFER P2P (INQ)");
			calltype = FWUtil.CALLTYPE_P2PINQ;
			URL_FF_case = "inqP2PTransaction";
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);			
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);

			FWUtil.log(LOGGER, "\nTRANSFER P2P (TRANS)");
			calltype = FWUtil.CALLTYPE_P2PTRANS;
			URL_FF_case = "execP2PTransaction";
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);
			response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}

	protected void test_TransferP2M(FWUser sender, FWUser recipient, String amount) {
		test_TransferP2M(sender, recipient, amount, null, null);
	}
	
	protected void test_TransferP2M(FWUser sender, FWUser recipient, String amount, String checkParam, String checkValue) {
		try {
			String calltype;
			String signature;
			List<NameValuePair> params;
			String URL_FF_case;
			
			FWUtil.log(LOGGER, "\nTRANSFER P2M (INQ)");
			URL_FF_case = "inqP2MTransaction";
			calltype = FWUtil.CALLTYPE_P2MINQ;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);

			FWUtil.log(LOGGER, "\nTRANSFER P2M (TRANS)");
			URL_FF_case = "execP2MTransaction";
			calltype = FWUtil.CALLTYPE_P2MTRANS;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);
			response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}

	protected void test_TransferBankNowInq(FWUser sender, String amount, String bankid, String norek) {
		test_TransferBankNowInq(sender, amount, bankid, norek, null, null);
	}
	
	protected void test_TransferBankNowInq(FWUser sender, String amount, String bankid, String norek, String checkParam, String checkValue) {
		try {
			FWUtil.log(LOGGER, "\nTRANSFER BANK REALTIME (INQ)"); // only doing inquiry
			String URL_FF_case = "inqBankTransferNow";
			String calltype = FWUtil.CALLTYPE_BANKTRFINQ;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, null, null, amount, bankid, norek, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}

	protected void test_TransferBankNowExec(FWUser sender, String amount, String bankid, String norek) {
		test_TransferBankNowExec(sender, amount, bankid, norek, null, null, null);
	}
	
	protected void test_TransferBankNowExec(FWUser sender, String amount, String bankid, String norek, String checkParam, String checkValueInq, String checkValueExec) {

		FWUtil.log(LOGGER, "\nTRANSFER BANK REALTIME (INQ + TRANS)"); // doing inquiry, followed by transaction
		
		try {
			String calltype;
			String signature;
			List<NameValuePair> params;
			String URL_FF_case;
			
			//URL_FF_case = "inqBankTransferNow";
			//calltype = FWUtil.CALLTYPE_BANKTRFINQ;
			//signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			//params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, null, null, amount, bankid, norek, null, null, null);
			//String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			//FWUtil.validateResponse(true, response, checkParam, checkValueInq, LOGGER);
			
			//JSONObject obj = new JSONObject(response);
			//String resultcode = obj.getString(FWUtil.PARAM_RESULTCODE);
			
			//if(resultcode.equals("100")) {
				//FWUtil.log(LOGGER, "Executing bank transfer now...");
				URL_FF_case = "execBankTransferNow";			
				calltype = FWUtil.CALLTYPE_BANKTRFTRANS;
				signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
				params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, null, null, amount, bankid, norek, null, null, null);
				String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
				FWUtil.validateResponse(true, response, checkParam, checkValueExec, LOGGER);				
			//} else {
			//	FWUtil.log(LOGGER, "Error at inquiry. Not executing bank transfer...");
			//}
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}	
	
	protected void test_TransactionHistory(FWUser user, String startdate, String enddate) {
		test_TransactionHistory(user, startdate, enddate, null, null);
	}
	
	protected void test_TransactionHistory(FWUser user, String startdate, String enddate, String checkParam, String checkValue) {
		try {
			FWUtil.log(LOGGER, "\nCHECK TRANSACTION HISTORY");
			String URL_FF_case = "getTransactionHistory";
			String calltype = FWUtil.CALLTYPE_TXHISTORY;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, startdate, enddate, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);

			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}
	}
	
	protected void test_TopupPulsa(FWUser sender, String amount, String nohpdest) {
		test_TopupPulsa(sender, amount, nohpdest, null, null);
	}
	
	protected void test_TopupPulsa(FWUser sender, String amount, String nohpdest, String checkParam, String checkValue) {
		try {
			String calltype;
			String signature;
			List<NameValuePair> params;
			String URL_FF_case;
			
			FWUtil.log(LOGGER, "\nTOPUP PULSA (INQ)");
			URL_FF_case = "inqTopupPulsa";
			calltype = FWUtil.CALLTYPE_TOPUPPULSAINQ;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, null, amount, null, null, null, null, nohpdest);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);

			FWUtil.log(LOGGER, "\nTOPUP PULSA (TRANS)");
			URL_FF_case = "execTopupPulsa";
			calltype = FWUtil.CALLTYPE_TOPUPPULSATRANS;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, null, amount, null, null, null, null, nohpdest);
			response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);
			
		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}		
	}

	/****************************************************************************************************************/
	/* INTERNAL API FUNCTIONS 																							*/
	/****************************************************************************************************************/
	protected void test_CashIn(FWUser user, String amount) {
		test_CashIn(user, amount, null, null);
	}
	
	protected void test_CashIn(FWUser user, String amount, String checkParam, String checkValue) {
		try {		
			FWUtil.log(LOGGER, "\nCASH IN (INTERNAL)");
			String calltype = FWUtil.CALLTYPE_INTERNAL_CASHIN;
			String URL_FF_case = "doCashIn";
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.nohp + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, amount, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);

		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}		
	}
	
	protected void test_ForceCloseAccount(FWUser user) {
		test_ForceCloseAccount(user, null, null);
	}
	
	protected void test_ForceCloseAccount(FWUser user, String checkParam, String checkValue) {
		try {		
			FWUtil.log(LOGGER, "\nFORCE CLOSE ACCOUNT (INTERNAL)");
			String calltype = FWUtil.CALLTYPE_INTERNAL_FORCECLOSEACCOUNT;
			String URL_FF_case = "forceCloseAccount";
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			String response = FWUtil.execute(URL_FF+URL_FF_case, new DefaultHttpClient(), new HttpPost(URL_FF+URL_FF_case), params, LOGGER);
			FWUtil.validateResponse(true, response, checkParam, checkValue, LOGGER);

		} catch (Exception e) {
			FWUtil.log(LOGGER, e);
		}		
	}
	
	/****************************************************************************************************************/
	/* HELPER FUNCTIONS 																							*/
	/****************************************************************************************************************/	
		
	private List<NameValuePair> buildParams(String calltype, String signature, String mid, 
											FWUser user,
												String newpin, String confpin, String flag, 
											String newnohp, String middest, String accountiddest, String amount,
											String bankid, String norek, String startdate, String enddate,
											String nohpdest
											) {				
		
		/*
		 *  Sample (all on): "1111-1111-1111-0000-0000"
		 *  index 0 | 1| 2| 3 = calltype | signature | mid 	 	| accountid
		 *  index 5 | 6| 7| 8 = nohp	 | name		 | email 	| dob
		 *  index 10|11|12|13 = pin	 	 | newpin	 | confpin 	| flag
		 *  index 15|16|17|18 = newnohp	 | middest	 | accountiddest | amount
		 *  index 20|21|22|23 = bankid	 | norek	 | startdate | enddate
		 *  index 24|25|26|27 = nohpdest |
		 */
		
	     String OnOff;
	     switch (calltype) {
         	 case FWUtil.CALLTYPE_GETMASTERDATA: 			// verified
         		 OnOff = "0000-0000-0000-0000-0000-0000"; break;	 	     
	         case FWUtil.CALLTYPE_REGISTRATION: 			// verified
	             OnOff = "0110-1111-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_LOGIN:					// todo
	        	 OnOff = "0111-0000-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_CHECKBALANCE: 			// verified
	         	 OnOff = "0111-0000-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_UPGRADETOREGISTEREDUSER: 	// verified
	         	 OnOff = "0111-0000-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_UPGRADETOMERCHANT: 		// verified
	         	 OnOff = "0111-0000-1000-0000-0000-0000"; break;	 
	         case FWUtil.CALLTYPE_CHANGEPIN:				// todo
	         	 OnOff = "0111-0000-1110-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_CHANGEPHONENUM: 			// verified
	         	 OnOff = "0111-1000-1000-1000-0000-0000"; break;
	         case FWUtil.CALLTYPE_CLOSEACCOUNT: 			// verified
	        	 OnOff = "0111-0000-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2PINQ: 					// verified + test add note
	        	 OnOff = "0111-0000-1000-0011-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2PTRANS: 				// verified + test add note
	        	 OnOff = "0111-0000-1000-0011-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2MINQ:					// verified
	        	 OnOff = "0111-0000-1000-0011-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2MTRANS:					// verified 
	        	 OnOff = "0111-0000-1000-0011-0000-0000"; break;
	         case FWUtil.CALLTYPE_BANKTRFINQ:				// verified
	        	 OnOff = "0111-0000-1000-0001-1100-0000"; break;
	         case FWUtil.CALLTYPE_BANKTRFTRANS:				// verified
	        	 OnOff = "0111-0000-1000-0001-1100-0000"; break;
	         case FWUtil.CALLTYPE_TXHISTORY: 				// verified
	        	 OnOff = "0111-0000-1000-0000-0011-0000"; break;
	         case FWUtil.CALLTYPE_TOPUPPULSAINQ:			// verified
	        	 OnOff = "0111-0000-1000-0001-1100-1000"; break;
	         case FWUtil.CALLTYPE_TOPUPPULSATRANS:			// verified
	        	 OnOff = "0111-0000-1000-0001-0000-1000"; break;
	         case FWUtil.CALLTYPE_INTERNAL_CASHIN:				// todo
	        	 OnOff = "0111-1000-0000-0001-0000-1000"; break;
	         case FWUtil.CALLTYPE_INTERNAL_FORCECLOSEACCOUNT:	// todo
	        	 OnOff = "0111-0000-1000-0000-0000-1000"; break;	 
	         default:
	             throw new IllegalArgumentException("Invalid calltype: " + calltype);
	     }    
	     
	     
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		
		if(OnOff.charAt(0) == '1')
			urlParameters.add(new BasicNameValuePair("calltype", calltype));
		if(OnOff.charAt(1) == '1')
			urlParameters.add(new BasicNameValuePair("signature", signature));
		if(OnOff.charAt(2) == '1')
			urlParameters.add(new BasicNameValuePair("mid", mid));
		if(OnOff.charAt(3) == '1')
			urlParameters.add(new BasicNameValuePair("accountid", user.accountID));
		
		
		if(OnOff.charAt(5) == '1')
			urlParameters.add(new BasicNameValuePair("nohp", user.nohp));
		if(OnOff.charAt(6) == '1')
			urlParameters.add(new BasicNameValuePair("name", user.name));
		if(OnOff.charAt(7) == '1')
			urlParameters.add(new BasicNameValuePair("email", user.email));
		if(OnOff.charAt(8) == '1')
			urlParameters.add(new BasicNameValuePair("dob", user.dob));
		
		
		if(OnOff.charAt(10) == '1')
			urlParameters.add(new BasicNameValuePair("pin", user.PIN));
		if(OnOff.charAt(11) == '1')
			urlParameters.add(new BasicNameValuePair("newpin", newpin));	
		if(OnOff.charAt(12) == '1')
			urlParameters.add(new BasicNameValuePair("confpin", confpin));	
		if(OnOff.charAt(13) == '1')
			urlParameters.add(new BasicNameValuePair("flag", flag));	
		
		
		if(OnOff.charAt(15) == '1')
			urlParameters.add(new BasicNameValuePair("newnohp", newnohp));
		if(OnOff.charAt(16) == '1')
			urlParameters.add(new BasicNameValuePair("middest", middest));
		if(OnOff.charAt(17) == '1')
			urlParameters.add(new BasicNameValuePair("accountiddest", accountiddest));		
		if(OnOff.charAt(18) == '1')
			urlParameters.add(new BasicNameValuePair("amount", amount));

		
		if(OnOff.charAt(20) == '1')
			urlParameters.add(new BasicNameValuePair("bankid", bankid));
		if(OnOff.charAt(21) == '1')
			urlParameters.add(new BasicNameValuePair("norek", norek));	
		if(OnOff.charAt(22) == '1')
			urlParameters.add(new BasicNameValuePair("startdate", startdate));
		if(OnOff.charAt(23) == '1')
			urlParameters.add(new BasicNameValuePair("enddate", enddate));	
		
		if(OnOff.charAt(25) == '1')
			urlParameters.add(new BasicNameValuePair("nohpdest", nohpdest));
	
		FWUtil.log(LOGGER, "Input: " + urlParameters.toString());
		
		return urlParameters;
	}
		
	public static void main(String[] args) throws Exception {

		
	}	
}
