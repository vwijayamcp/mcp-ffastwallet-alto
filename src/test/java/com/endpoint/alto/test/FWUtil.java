package com.endpoint.alto.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

public class FWUtil {
	
	public static final String USER_AGENT = "Mozilla/5.0";	
	public static final String ALG_SHA1 = "SHA-1";
	public static final String ALG_SHA256 = "SHA-256";

	public static final String CALLTYPE_GETMASTERDATA = "MASTERDATA";
	public static final String CALLTYPE_REGISTRATION = "REGISTRATION";
	public static final String CALLTYPE_LOGIN = "LOGIN";
	public static final String CALLTYPE_CHECKBALANCE = "SALDO";
	public static final String CALLTYPE_UPGRADETOREGISTEREDUSER = "CHANGEPRODUCT";
	public static final String CALLTYPE_UPGRADETOMERCHANT = "CHANGEPRODUCTMERCHANT";
	public static final String CALLTYPE_CHANGEPHONENUM = "CHANGENOHP";
	public static final String CALLTYPE_CHANGEPIN = "CHANGEPIN";
	public static final String CALLTYPE_CLOSEACCOUNT = "CLOSEACCOUNT";
	public static final String CALLTYPE_P2PINQ = "TRANSFERINQ";
	public static final String CALLTYPE_P2PTRANS = "TRANSFERTRANS";
	public static final String CALLTYPE_P2MINQ = "TRANSFERMERCHANTINQ";
	public static final String CALLTYPE_P2MTRANS = "TRANSFERMERCHANTTRANS";
	public static final String CALLTYPE_BANKTRFINQ = "TRANSFERBANKINQ";
	public static final String CALLTYPE_BANKTRFTRANS = "TRANSFERBANKTRANS";
	public static final String CALLTYPE_TXHISTORY = "LASTTRANS";
	public static final String CALLTYPE_TOPUPPULSAINQ = "TOPUPOTHERINQ";
	public static final String CALLTYPE_TOPUPPULSATRANS = "TOPUPOTHERTRANS";
	public static final String CALLTYPE_INTERNAL_CASHIN = "SETORTUNAI";
	public static final String CALLTYPE_INTERNAL_FORCECLOSEACCOUNT = "CLOSEACCOUNT2";
	
	final static String PARAM_ACCOUNTID = "accountid";
	final static String PARAM_RESULTCODE = "resultcode";
	final static String PARAM_SALDO = "saldo";
	
	final static int OVERTIME_LIMIT = 10000;
	
	public static boolean LOG_WRITE = false;

	public FWUtil() {
		// TODO Auto-generated constructor stub
	}
	
	public static String execute(String url, HttpClient client, HttpPost post, List<NameValuePair> params, Logger logger) throws Exception {

		// add header
		post.setHeader("User-Agent", FWUtil.USER_AGENT);	
		
		if(params != null)
			post.setEntity(new UrlEncodedFormEntity(params));

		long start_time = System.nanoTime();
		HttpResponse response = client.execute(post);
		long end_time = System.nanoTime();
		double difference = (end_time - start_time)/1e6; // execution time in milliseconds
		
		if(!LOG_WRITE)
			System.out.println("Sending 'POST' request to URL : " + url);
		else
			logger.info("Sending 'POST' request to URL : " + url);
		//System.out.println("Post parameters : " + post.getEntity());
		//System.out.println("Response Code : " + response.getStatusLine());
		
		BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		String answer = result.toString();
		String OVERTIME = "";
		if(!LOG_WRITE) {
			System.out.println(answer);
			if (difference > OVERTIME_LIMIT)
				OVERTIME = "OVERTIME! ";
			System.out.println(OVERTIME + "Execution time (ms): " + difference);
		} else {
			logger.info(answer);
			if (difference > OVERTIME_LIMIT)
				OVERTIME = "OVERTIME! ";
			logger.info(OVERTIME + "Execution time (ms): " + difference);
		}
		
		return answer;
	}
	
	public static void validateResponse(Boolean check, String response, String param, String expectedValue, Logger logger) {
		
		String resultcode = null;
		String realValue = null;
		
		if(!check)
			return;
		
		try {
			JSONObject obj = new JSONObject(response);
			resultcode = obj.getString(PARAM_RESULTCODE);
			
			if(resultcode.equals("100")) {
				if(param == null) {
					FWUtil.log(logger, "PASS");
				} else {
					realValue = obj.getString(param);
					if(realValue.equals(expectedValue)) {
						FWUtil.log(logger, "PASS");						
					} else {
						FWUtil.log(logger, "PROBLEM FOUND! " + param + " Expected value: " + expectedValue + " ; Real value: " + realValue);
					}
				}
			} else {
				if(param == PARAM_RESULTCODE) {
					resultcode = obj.getString(PARAM_RESULTCODE);				
					if(resultcode.equals(expectedValue)) {
						FWUtil.log(logger, "PASS" + " Expected error resultcode: " + resultcode);
					} else {
						FWUtil.log(logger, "PROBLEM FOUND! " +  PARAM_RESULTCODE +  " Expected: " + expectedValue + " ; Real: " + resultcode);
					}
				} else {
					FWUtil.log(logger, "PROBLEM FOUND!" + " Result Code: " + resultcode);				
				}				
			}			
		} catch (Exception e) {
			FWUtil.log(logger, "PROBLEM FOUND:\n" + e);
		}	
	}
	
	private static boolean isResultCodeMatch(String realValue, String expectedValues) {
		
		boolean match = false;
		String[] expectedValuesOpt = null;
		
		if (expectedValues.contains(",")) {
			expectedValuesOpt = expectedValues.split(",");
		} else {
			expectedValuesOpt[0] = expectedValues;
		}
		
		for (int i=0; i<expectedValuesOpt.length; i++) {
			if(expectedValuesOpt[i].equals(realValue))
				match = true;		
		}
		
		return match;		
	}

	public static String createSignature(String alg, String x) {
		
		try {
			MessageDigest md = MessageDigest.getInstance(alg);
			md.update(x.getBytes());
			byte[] sha256 = md.digest();
			return byteArrayToHex(sha256);
		 } catch (Exception e) {
			return "error";
		 }
	}
	
	private static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder();
		for(byte b: a)
			sb.append(String.format("%02x", b&0xff));
		return sb.toString();
	}
	
	public static String getAccountIDInfo(String response) {
		
		String accountid = null;
		
		try {
			JSONObject obj = new JSONObject(response);
			accountid = obj.getString(PARAM_ACCOUNTID);
		} catch (Exception e) {
			System.out.println(e);
		}

		return accountid;			
	}
	
	public static void log(Logger logger, String msg) {
		if(!FWUtil.LOG_WRITE)
			System.out.println(msg);
		else
			logger.info(msg);
	}
	
	public static void log(Logger logger, Exception e) {
		if(!FWUtil.LOG_WRITE)
			System.out.println(e);
		else
			logger.info(e.toString());		
	}
	
	public static Logger createLogger(String path, String filename) {
	    Logger logger = Logger.getLogger("MyLog");  
	    FileHandler fh;  

	    try {  

	        // This block configure the logger with handler and formatter  
	        fh = new FileHandler(path + filename);  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  

	        // the following statement is used to log any messages  
	        //logger.info("My first log");


	    } catch (SecurityException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  

	    return logger;
	    //logger.info("Hi How r u?");  
	}
}
