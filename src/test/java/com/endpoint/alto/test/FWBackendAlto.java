 package com.endpoint.alto.test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class FWBackendAlto {

	String URL_REG 	= "/mcpayment/api/reg_customer.jsp";
	String URL_DATA = "/mcpayment/api/sync_data.jsp";
	String URL_SVC	= "/mcpayment/api/services.jsp";
	
	String ID_MERCHANT;
	String KEY_ALTO;
	String KEY_MERCHANT;
	
	Logger logger;

	FWBackendAlto(String mid, String signatureKey, String host, Logger logger) {
		this.ID_MERCHANT = mid;
		this.KEY_ALTO = signatureKey;
		this.KEY_MERCHANT = FWUtil.createSignature(FWUtil.ALG_SHA1, KEY_ALTO + ID_MERCHANT);
		this.URL_REG = host + URL_REG;
		this.URL_DATA = host + URL_DATA;
		this.URL_SVC = host + URL_SVC;
		this.logger = logger;
	}
		
	/****************************************************************************************************************/
	/* TEST STEP IMPLEMENTATION																						*/
	/****************************************************************************************************************/	

	protected void test_GetMasterData() {
		
		try {
			System.out.println("\nGET MASTER DATA");
			FWUtil.execute(URL_DATA, new DefaultHttpClient(), new HttpPost(URL_DATA), null, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected String test_Registration(FWUser user) {

		String accountid = null;
		
		try {			
			System.out.println("\nREGISTRATION");
			String calltype = FWUtil.CALLTYPE_REGISTRATION;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.nohp + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);

			String response = FWUtil.execute(URL_REG, new DefaultHttpClient(), new HttpPost(URL_REG), params, logger);			
			accountid = FWUtil.getAccountIDInfo(response);
			System.out.println("Account ID: " + accountid);			
			
		} catch (Exception e) {
			System.out.println(e);
		}

		return accountid;
	}
	
	protected void test_Login(FWUser user) {
		
		try {
			System.out.println("\nLOGIN");
			String calltype = FWUtil.CALLTYPE_LOGIN;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected void test_UpgradeUserToRegistered(FWUser user) {
		
		try {
			String calltype = FWUtil.CALLTYPE_UPGRADETOREGISTEREDUSER;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params;
			
			System.out.println("\nCHANGE USER LEVEL UNREGISTERED TO REGISTERED (INQ)");
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "1", null, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
			System.out.println("\nCHANGE USER LEVEL UNREGISTERED TO REGISTERED (TRANS)");
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "2", null, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);	
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected void test_UpgradeUserToMerchant(FWUser user) {
		
		try {
			String calltype = FWUtil.CALLTYPE_UPGRADETOMERCHANT;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params;
			
			System.out.println("\nCHANGE USER LEVEL UNREGISTERED TO MERCHANT (INQ)");
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "1", null, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
			System.out.println("\nCHANGE USER LEVEL UNREGISTERED TO MERCHANT (TRANS)");
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "2", null, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);	
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}	
	
	protected void test_CheckBalance(FWUser user) {
		try {
			System.out.println("\nCHECK BALANCE");
			String calltype = FWUtil.CALLTYPE_CHECKBALANCE;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	protected void test_ChangePhoneNumber(FWUser user, String newphone) {
		try {
			String calltype = FWUtil.CALLTYPE_CHANGEPHONENUM;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params;
			
			System.out.println("\nCHANGE NO HP (INQ)");
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "1", newphone, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
			System.out.println("\nCHANGE NO HP (TRANS)");
			params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, "2", newphone, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);	
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected void test_ChangePIN(FWUser user, String newpin) {
		try {		
			System.out.println("\nCHANGE PIN");
			String calltype = FWUtil.CALLTYPE_CHANGEPIN;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, newpin, newpin, null, null, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);			
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected void test_CloseAccount(FWUser user) {
		try {		
			System.out.println("\nCLOSE ACCOUNT");
			String calltype = FWUtil.CALLTYPE_CLOSEACCOUNT;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);			
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected void test_TransferP2P(FWUser sender, FWUser recipient, String amount) {
		try {					
			System.out.println("\nTRANSFER P2P (INQ)");
			String calltype = FWUtil.CALLTYPE_P2PINQ;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);

			System.out.println("\nTRANSFER P2P (TRANS)");
			calltype = FWUtil.CALLTYPE_P2PTRANS;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	protected void test_TransferP2M(FWUser sender, FWUser recipient, String amount) {
		try {
			System.out.println("\nTRANSFER P2M (INQ)");
			String calltype = FWUtil.CALLTYPE_P2MINQ;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);

			System.out.println("\nTRANSFER P2M (TRANS)");
			calltype = FWUtil.CALLTYPE_P2MTRANS;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, ID_MERCHANT, recipient.accountID, amount, null, null, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	protected void test_TransferBankRealTime(FWUser sender, String amount, String bankid, String norek) {
		try {
			System.out.println("\nTRANSFER BANK REALTIME (INQ)");
			String calltype = FWUtil.CALLTYPE_BANKTRFINQ;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, null, null, amount, bankid, norek, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);

			System.out.println("\nTRANSFER BANK REALTIME (TRANS)");
			calltype = FWUtil.CALLTYPE_BANKTRFTRANS;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, null, null, amount, bankid, norek, null, null, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected void test_TransactionHistory(FWUser user, String startdate, String enddate) {
		try {
			System.out.println("\nCHECK TRANSACTION HISTORY");
			String calltype = FWUtil.CALLTYPE_TXHISTORY;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + user.accountID + user.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, user, null, null, null, null, null, null, null, null, null, startdate, enddate, null);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	protected void test_TopupPulsa(FWUser sender, String amount, String nohpdest) {
		try {
			System.out.println("\nTOPUP PULSA (INQ)");
			String calltype = FWUtil.CALLTYPE_TOPUPPULSAINQ;
			String signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			List<NameValuePair> params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, null, null, amount, null, null, null, null, nohpdest);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);

			System.out.println("\nTOPUP PULSA (TRANS)");
			calltype = FWUtil.CALLTYPE_TOPUPPULSATRANS;
			signature = FWUtil.createSignature(FWUtil.ALG_SHA256, calltype + KEY_MERCHANT + sender.accountID + sender.PIN);
			params = buildParams(calltype, signature, ID_MERCHANT, sender, null, null, null, null, null, null, amount, null, null, null, null, nohpdest);
			FWUtil.execute(URL_SVC, new DefaultHttpClient(), new HttpPost(URL_SVC), params, logger);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/****************************************************************************************************************/
	/* HELPER FUNCTIONS 																							*/
	/****************************************************************************************************************/	

	private void log(String msg) {
		if(!FWUtil.LOG_WRITE)
			System.out.println(msg);
		else
			logger.info(msg);
	}
	
	private void log(Exception e) {
		if(!FWUtil.LOG_WRITE)
			System.out.println(e);
		else
			logger.info(e.toString());		
	}
	
	private List<NameValuePair> buildParams(String calltype, String signature, String mid, 
											FWUser user,
												String newpin, String confpin, String flag, 
											String newnohp, String middest, String accountiddest, String amount,
											String bankid, String norek, String startdate, String enddate,
											String nohpdest
											) {				
		
		/*
		 *  Sample (all on): "1111-1111-1111-0000-0000"
		 *  index 0 | 1| 2| 3 = calltype | signature | mid 	 	| accountid
		 *  index 5 | 6| 7| 8 = nohp	 | name		 | email 	| dob
		 *  index 10|11|12|13 = pin	 	 | newpin	 | confpin 	| flag
		 *  index 15|16|17|18 = newnohp	 | middest	 | accountiddest | amount
		 *  index 20|21|22|23 = bankid	 | norek	 | startdate | enddate
		 *  index 24|25|26|27 = nohpdest |
		 */

	     String OnOff;
	     switch (calltype) {
	         case FWUtil.CALLTYPE_REGISTRATION:
	             OnOff = "1110-1111-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_LOGIN:
	        	 OnOff = "1111-0000-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_CHECKBALANCE:
	         	 OnOff = "1111-0000-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_UPGRADETOREGISTEREDUSER:
	         	 OnOff = "1111-0000-1001-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_UPGRADETOMERCHANT:
	         	 OnOff = "1111-0000-1001-0000-0000-0000"; break;	 
	         case FWUtil.CALLTYPE_CHANGEPIN:
	         	 OnOff = "1111-0000-1110-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_CHANGEPHONENUM:
	         	 OnOff = "1111-1000-1001-1000-0000-0000"; break;
	         case FWUtil.CALLTYPE_CLOSEACCOUNT:
	        	 OnOff = "1111-0000-1000-0000-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2PINQ:
	        	 OnOff = "1111-0000-1000-0111-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2PTRANS:
	        	 OnOff = "1111-0000-1000-0111-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2MINQ:
	        	 OnOff = "1111-0000-1000-0111-0000-0000"; break;
	         case FWUtil.CALLTYPE_P2MTRANS:
	        	 OnOff = "1111-0000-1000-0111-0000-0000"; break;
	         case FWUtil.CALLTYPE_BANKTRFINQ:
	        	 OnOff = "1111-0000-1000-0001-1100-0000"; break;
	         case FWUtil.CALLTYPE_BANKTRFTRANS:
	        	 OnOff = "1111-0000-1000-0001-1100-0000"; break;
	         case FWUtil.CALLTYPE_TXHISTORY:
	        	 OnOff = "1111-0000-1000-0000-0011-0000"; break;
	         case FWUtil.CALLTYPE_TOPUPPULSAINQ:			
	        	 OnOff = "1111-0000-1000-0001-1100-1000"; break;
	         case FWUtil.CALLTYPE_TOPUPPULSATRANS:			
	        	 OnOff = "1111-0000-1000-0001-0000-1000"; break;
	         default:
	             throw new IllegalArgumentException("Invalid calltype: " + calltype);
	     }    
	     
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		
		if(OnOff.charAt(0) == '1')
			urlParameters.add(new BasicNameValuePair("calltype", calltype));
		if(OnOff.charAt(1) == '1')
			urlParameters.add(new BasicNameValuePair("signature", signature));
		if(OnOff.charAt(2) == '1')
			urlParameters.add(new BasicNameValuePair("mid", mid));
		if(OnOff.charAt(3) == '1')
			urlParameters.add(new BasicNameValuePair("accountid", user.accountID));
		
		
		if(OnOff.charAt(5) == '1')
			urlParameters.add(new BasicNameValuePair("nohp", user.nohp));
		if(OnOff.charAt(6) == '1')
			urlParameters.add(new BasicNameValuePair("name", user.name));
		if(OnOff.charAt(7) == '1')
			urlParameters.add(new BasicNameValuePair("email", user.email));
		if(OnOff.charAt(8) == '1')
			urlParameters.add(new BasicNameValuePair("dob", user.dob));
		
		
		if(OnOff.charAt(10) == '1')
			urlParameters.add(new BasicNameValuePair("pin", user.PIN));
		if(OnOff.charAt(11) == '1')
			urlParameters.add(new BasicNameValuePair("newpin", newpin));	
		if(OnOff.charAt(12) == '1')
			urlParameters.add(new BasicNameValuePair("confpin", confpin));	
		if(OnOff.charAt(13) == '1')
			urlParameters.add(new BasicNameValuePair("flag", flag));	
		
		
		if(OnOff.charAt(15) == '1')
			urlParameters.add(new BasicNameValuePair("newnohp", newnohp));
		if(OnOff.charAt(16) == '1')
			urlParameters.add(new BasicNameValuePair("middest", middest));
		if(OnOff.charAt(17) == '1')
			urlParameters.add(new BasicNameValuePair("accountiddest", accountiddest));		
		if(OnOff.charAt(18) == '1')
			urlParameters.add(new BasicNameValuePair("amount", amount));

		
		if(OnOff.charAt(20) == '1')
			urlParameters.add(new BasicNameValuePair("bankid", bankid));
		if(OnOff.charAt(21) == '1')
			urlParameters.add(new BasicNameValuePair("norek", norek));	
		if(OnOff.charAt(22) == '1')
			urlParameters.add(new BasicNameValuePair("startdate", startdate));
		if(OnOff.charAt(23) == '1')
			urlParameters.add(new BasicNameValuePair("enddate", enddate));		
		
		if(OnOff.charAt(25) == '1')
			urlParameters.add(new BasicNameValuePair("nohpdest", nohpdest));
		
		log("Input: " + urlParameters.toString());
		
		return urlParameters;
	}	
	

		
	/****************************************************************************************************************/
	/* TEST CASE IMPLEMENTATION																						*/
	/****************************************************************************************************************/	
	
	public static void testCase0(FWBackendAlto w) {
		
		System.out.println("----- TEST CASE 0 -----");
		w.test_GetMasterData();		
	}
	
	public static void testCase1(FWBackendAlto w, FWUser u) {
		
		System.out.println("----- TEST CASE 1 -----");
		w.test_Registration(u);	
	}
	
	public static void testCase2(FWBackendAlto w, FWUser u) {
		
		System.out.println("----- TEST CASE 2 -----");
		w.test_Login(u);
		w.test_UpgradeUserToRegistered(u);
		w.test_UpgradeUserToRegistered(u);	// repeat
		w.test_ChangePhoneNumber(u, "081287654321");
		w.test_ChangePhoneNumber(u, "081287654321"); // repeat
		w.test_CheckBalance(u);	
	}
	
	public static void testCase3(FWBackendAlto w, FWUser u) {
		
		System.out.println("----- TEST CASE 3 -----");
		w.test_Login(u);
		w.test_ChangePIN(u, "111111");
		w.test_CheckBalance(u);		//expect: wrong pin (1/3)
		u.PIN = "111111";
		w.test_CheckBalance(u);		//expect: success
		u.PIN = "000000";
		w.test_CheckBalance(u);		//expect: wrong pin (1/3)
		u.PIN = "111111";
		w.test_CheckBalance(u);		//expect: success
		w.test_ChangePIN(u, "000000");	//expect: cannot use the last 4 -------> but succeed!
		w.test_ChangePIN(u, "111111");	//expect: fail. it's the current PIN.
		w.test_ChangePIN(u, "222222");	//expect: success
		w.test_ChangePIN(u, "333333");	//expect: success
		w.test_ChangePIN(u, "000000");	//expect: success
	}
	
	public static void testCase4(FWBackendAlto w, FWUser u) {

		System.out.println("----- TEST CASE 4 -----");
		w.test_Login(u);
		w.test_CloseAccount(u);
	}	
	
}