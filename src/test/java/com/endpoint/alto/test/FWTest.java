package com.endpoint.alto.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;

public class FWTest {

	public FWTest() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {

		//String ALTO_DEV_HOST = "http://202.158.48.243:8899";
		//String ALTO_DEV_KEY = "3K0134526787YSGYSGYT2Y86";
		//String ALTO_SAND_HOST = "http://202.158.48.243:3113";
		//String ALTO_SAND_KEY = "3K0444G5666Y6T4D4E3N7K8L";
		
		//String FF_DEV_HOST = "http://wa1a.dev.id.mcpayment.net/Ffastwallet/v2";
		//String FF_DEV_KEY = "1111TEST1234567890";
		String FF_SAND_HOST = "http://wa1a.sandbox.id.mcpayment.net/Ffastwallet/v2";
		//String FF_SAND_HOST = "http://192.168.1.120:8080/Ffastwallet/v2";
		String FF_SAND_KEY = "aX9zGdZBHHLmFO4yj9sJ0joiyq4f8";
		//String FF_SAND_KEY = "L79zGdZBSSSmFO4yj9sJ07WUyqPM0";
		
		Logger logger = null;
		FWUtil.LOG_WRITE = true;
		if (FWUtil.LOG_WRITE)
			logger = FWUtil.createLogger("/Users/valerino/Desktop/", "log.txt");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		System.out.println("\nTEST STARTED AT: " + dateFormat.format(cal.getTime()));
		
		String merchantID = "2";
		
		long start_time = System.nanoTime();
		test_frontend(merchantID, FF_SAND_KEY, FF_SAND_HOST, logger);
		//test_backend(merchantID, ALTO_DEV_KEY, ALTO_DEV_HOST);
		long end_time = System.nanoTime();
		double difference = (end_time - start_time)/1e9; // execution time in seconds
		
		System.out.println("\nTEST DURATION(s): " + difference);
		
		cal = Calendar.getInstance();
		System.out.println("\nTEST COMPLETED AT: " + dateFormat.format(cal.getTime()));	
	}
	
	private static void test_frontend(String merchantID, String key, String host, Logger logger) {
				
		FWFrontend w_frontend = new FWFrontend(merchantID, key, host, logger);
		
		test_frontend_case0(w_frontend, logger);	// Get Master Data
		
		test_frontend_case1(w_frontend, logger);	// Registration, Check Balance, Change Phone, Close Account	
		
		test_frontend_case2(w_frontend, logger);	// Upgrade to Registered, Upgrade to Merchant, Close Account	
		
		test_frontend_case3(w_frontend, logger);	// P2P, Transaction History
		
		test_frontend_case4(w_frontend, logger);	// P2M, Transaction History
		
		test_frontend_case5(w_frontend, logger);	// Invalid identifier, params
		
		test_frontend_case6(w_frontend, logger);	// Topup pulsa, Transaction History
		
		test_frontend_case7(w_frontend, logger);	// Bank transfer, Transaction History		
		
	}

	/*****************
	 * Get Master Data
	 *****************/
	private static void test_frontend_case0(FWFrontend f, Logger logger) {
		
		FWUtil.log(logger, "\n------------- TEST CASE 0 START -------------");
		
		f.test_GetMasterData();
	}
	
	/*****************
	 * Registration, Check Balance, Change Phone
	 *****************/
	private static void test_frontend_case1(FWFrontend f, Logger logger) {
		
		FWUtil.log(logger, "\n------------- TEST CASE 1 START -------------");
	
		// Create & Register user1 (unregistered)
		String myName	= "TesterZ01";
		String myEmail	= "tester.z01@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP 	= "081311000011";
		String myPIN 	= "000000";		
		FWUser user1 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user1.accountID = f.test_Registration(user1);

		// Check balance user1
		f.test_CheckBalance(user1, FWUtil.PARAM_SALDO, "0");
		// Top up balance: Rp 200,000 ----- manual
		f.test_CashIn(user1, "200000");
		// Confirm balance user1
		f.test_CheckBalance(user1, FWUtil.PARAM_SALDO, "200000");
		
		// Change phone number user 1
		String oldnum = myNoHP;
		String newnum = myNoHP + "9";
		f.test_ChangePhoneNumber(user1, newnum);
		f.test_ChangePhoneNumber(user1, oldnum);
		
		// Confirm phone number of user 1
		
		// Delete user1
		f.test_ForceCloseAccount(user1);
		f.test_CloseAccount(user1, FWUtil.PARAM_RESULTCODE, "50");
	}

	/*****************
	 * Upgrade to Registered, Upgrade to Merchant
	 *****************/
	private static void test_frontend_case2(FWFrontend f, Logger logger) {
		
		FWUtil.log(logger, "\n------------- TEST CASE 2 START -------------");
		
		// Create & Register user1 (unregistered), user2 (unregistered->registered), user3 (registered->merchant), user4 (unregistered->merchant)
		String myName	= "TesterZ01";
		String myEmail	= "tester.z01@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP 	= "081320000011";
		String myPIN 	= "000000";		
		FWUser user1 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user1.accountID = f.test_Registration(user1);
		
		myName	= "TesterZ02";
		myEmail	= "tester.z02@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081312000012";
		myPIN 	= "000000";		
		FWUser user2 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user2.accountID = f.test_Registration(user2);

		myName	= "TesterZ03";
		myEmail	= "tester.z03@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081320000013";
		myPIN 	= "000000";		
		FWUser user3 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user3.accountID = f.test_Registration(user3);		

		myName	= "TesterZ04";
		myEmail	= "tester.z04@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081320000014";
		myPIN 	= "000000";		
		FWUser user4 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user4.accountID = f.test_Registration(user4);	
		
		// Upgrade to Registered user2, user3
		f.test_UpgradeUserToRegistered(user2);
		f.test_UpgradeUserToRegistered(user3);
		
		// Upgrade to Registered user2 (expect error: already registered)
		f.test_UpgradeUserToRegistered(user2, FWUtil.PARAM_RESULTCODE, "325");
		
		// Upgrade to Merchant user4, user3
		f.test_UpgradeUserToMerchant(user4);
		f.test_UpgradeUserToMerchant(user3);
		
		// Upgrade to Merchant user3 (expect error: already merchant)
		f.test_UpgradeUserToMerchant(user3, FWUtil.PARAM_RESULTCODE, "343");
		
		// Upgrade to Registered user4 (expect error: cannot change from Registered -> Merchant)
		f.test_UpgradeUserToRegistered(user4, FWUtil.PARAM_RESULTCODE, "325");
		
		// Confirm level of user1, user2, user3, user4
		
		// Delete user1, user2, user3, user4	
		f.test_CloseAccount(user1);
		f.test_CloseAccount(user2);
		f.test_CloseAccount(user3);
		f.test_CloseAccount(user4);
	}	

	/*****************
	 * P2P, Transaction History
	 *****************/	
	private static void test_frontend_case3(FWFrontend f, Logger logger) {

		FWUtil.log(logger, "\n------------- TEST CASE 3 START -------------");
		
		// Create & Register user1 (unregistered), user2 (unregistered->registered), user3 (registered->merchant), user4 (unregistered->merchant)
		String myName	= "TesterZ01";
		String myEmail	= "tester.z01@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP 	= "081330000011";
		String myPIN 	= "000000";		
		FWUser user1 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user1.accountID = f.test_Registration(user1);
		
		myName	= "TesterZ02";
		myEmail	= "tester.z02@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081330000012";
		myPIN 	= "000000";		
		FWUser user2 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user2.accountID = f.test_Registration(user2);

		myName	= "TesterZ03";
		myEmail	= "tester.z03@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081330000013";
		myPIN 	= "000000";		
		FWUser user3 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user3.accountID = f.test_Registration(user3);		

		myName	= "TesterZ04";
		myEmail	= "tester.z04@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081330000014";
		myPIN 	= "000000";		
		FWUser user4 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user4.accountID = f.test_Registration(user4);	
		
		// Upgrade to Registered user2, user3
		f.test_UpgradeUserToRegistered(user2);
		f.test_UpgradeUserToRegistered(user3);
		
		// P2P transfer user1 -> user2: Rp 200,000 (expect error: unreg not allowed to transfer)
		//TEMPORARY f.test_TransferP2P(user1, user2, "200000", FWUtil.PARAM_RESULTCODE, "336");
		// P2P transfer user2 -> user1: Rp 200,000 (expect error: insufficient balance)
		f.test_TransferP2P(user2, user1, "200000", FWUtil.PARAM_RESULTCODE, "955");
			
		// Top up balance: Rp 1,100,000 (expect error: recipient over limit: unregistered Rp 1 million)
		f.test_CashIn(user1, "1100000", FWUtil.PARAM_RESULTCODE, "959");
		// Top up balance: Rp   900,000 ----- manual: user1
		f.test_CashIn(user1,  "900000");
		// Top up balance: Rp 5,000,000 ----- manual: user2
		f.test_CashIn(user2, "5000000");
		
		// P2P transfer user1 -> user2: Rp 10,000 (expect error: unreg not allowed to transfer)
		//TEMPORARY f.test_TransferP2P(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "336");
		// P2P transfer user1 -> user3: Rp 10,000 (expect error: unreg not allowed to transfer)
		//TEMPORARY f.test_TransferP2P(user1, user3, "10000", FWUtil.PARAM_RESULTCODE, "336");
		// P2P transfer user1 -> user4: Rp 10,000 (expect error: unreg not allowed to transfer)
		//TEMPORARY f.test_TransferP2P(user1, user4, "10000", FWUtil.PARAM_RESULTCODE, "336");
		
		// P2P transfer user2 -> user1: Rp 200,000 (expect error: recipient over limit)
		f.test_TransferP2P(user2, user1, "200000", FWUtil.PARAM_RESULTCODE, "959");
		// P2P transfer user2 -> user4: Rp 10,000
		f.test_TransferP2P(user2, user4, "10000");
		// P2P transfer user2 -> user3: Rp 10,000
		f.test_TransferP2P(user2, user3, "10000");
		// Upgrade to Merchant user3, user 4
		f.test_UpgradeUserToMerchant(user3);
		f.test_UpgradeUserToMerchant(user4);		
		// P2P transfer user2 -> user3: Rp 100,000
		f.test_TransferP2P(user2, user3, "100000");
		
		// P2P transfer user3 -> user1: Rp 10,000
		f.test_TransferP2P(user3, user1, "10000");
		// P2P transfer user3 -> user2: Rp 10,000
		f.test_TransferP2P(user3, user2, "10000");
		// P2P transfer user3 -> user4: Rp 30,000
		f.test_TransferP2P(user3, user4, "30000");
		
		// Get transaction history user1, user2, user3, user4
		f.test_TransactionHistory(user1, "01/08/2016", "31/08/2016");
		f.test_TransactionHistory(user2, "01/08/2016", "31/08/2016");
		f.test_TransactionHistory(user3, "01/08/2016", "31/08/2016");
		f.test_TransactionHistory(user4, "01/08/2016", "31/08/2016");
		
		// Confirm balance: user 1 Rp. 910,000
		// Confirm balance: user 2 Rp. 4,890,000
		// Confirm balance: user 3 Rp. 60,000
		// Confirm balance: user 4 Rp. 40,000
		f.test_CheckBalance(user1, FWUtil.PARAM_SALDO, "910000");
		f.test_CheckBalance(user2, FWUtil.PARAM_SALDO, "4890000");
		f.test_CheckBalance(user3, FWUtil.PARAM_SALDO, "60000");
		f.test_CheckBalance(user4, FWUtil.PARAM_SALDO, "40000");
		
		// Delete user1, user2, user3, user4
		f.test_ForceCloseAccount(user1);
		f.test_ForceCloseAccount(user2);
		f.test_ForceCloseAccount(user3);
		f.test_ForceCloseAccount(user4);
	}	

	/*****************
	 * P2M, Transaction History
	 *****************/
	private static void test_frontend_case4(FWFrontend f, Logger logger) {
		
		FWUtil.log(logger, "\n------------- TEST CASE 4 START -------------");

		// Create & Register user1 (unregistered), user2 (unregistered->registered), user3 (registered->merchant), user4 (unregistered->registered->merchant)
		String myName	= "TesterZ01";
		String myEmail	= "tester.z01@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP 	= "081340000011";
		String myPIN 	= "000000";		
		FWUser user1 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user1.accountID = f.test_Registration(user1);
		
		myName	= "TesterZ02";
		myEmail	= "tester.z02@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081340000012";
		myPIN 	= "000000";		
		FWUser user2 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user2.accountID = f.test_Registration(user2);

		myName	= "TesterZ03";
		myEmail	= "tester.z03@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081340000013";
		myPIN 	= "000000";		
		FWUser user3 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user3.accountID = f.test_Registration(user3);		

		myName	= "TesterZ04";
		myEmail	= "tester.z04@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081340000014";
		myPIN 	= "000000";		
		FWUser user4 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user4.accountID = f.test_Registration(user4);	
				

		// Upgrade to Registered user2
		f.test_UpgradeUserToRegistered(user2);
		// Upgrade to Merchant user3
		f.test_UpgradeUserToMerchant(user3);

		// P2P transfer user3 -> user2: Rp 200,000 (expect error: insufficient balance)
		f.test_TransferP2P(user3, user2, "200000", FWUtil.PARAM_RESULTCODE, "955");
		
		// Top up balance: Rp   900,000 ----- manual: user1
		f.test_CashIn(user1,  "900000");
		// Top up balance: Rp 5,000,000 ----- manual: user2
		f.test_CashIn(user2, "5000000");

		// P2M transfer user1 -> user4: Rp 10,000 (expect error: recipient not merchant)
		f.test_TransferP2M(user1, user4, "10000", FWUtil.PARAM_RESULTCODE, "337");
		// P2M transfer user1 -> user2: Rp 10,000 (expect error: recipient not merchant)
		f.test_TransferP2M(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "337");
		// P2M transfer user1 -> user3: Rp 50,000
		f.test_TransferP2M(user1, user3, "50000");

		// P2M transfer user2 -> user1: Rp 10,000 (expect error: recipient not merchant)
		f.test_TransferP2M(user2, user1, "10000", FWUtil.PARAM_RESULTCODE, "337");
		// Upgrade to Registered user4
		f.test_UpgradeUserToRegistered(user4);
		// P2M transfer user2 -> user4: Rp 10,000 (expect error: recipient not merchant)
		f.test_TransferP2M(user2, user4, "10000", FWUtil.PARAM_RESULTCODE, "337");
		// P2M transfer user2 -> user3: Rp 50,000
		f.test_TransferP2M(user2, user3, "50000");

		// P2M transfer user3 -> user1: Rp 10,000 (expect error: recipient not merchant)
		f.test_TransferP2M(user3, user1, "10000", FWUtil.PARAM_RESULTCODE, "337");
		// P2M transfer user3 -> user2: Rp 10,000 (expect error: recipient not merchant)
		f.test_TransferP2M(user3, user2, "10000", FWUtil.PARAM_RESULTCODE, "337");
		// Upgrade to Merchant user4
		f.test_UpgradeUserToMerchant(user4);
		// P2M transfer user3 -> user4: Rp 30,000
		f.test_TransferP2M(user3, user4, "30000");
		
		// Get transaction history user1, user2, user3, user4
		f.test_TransactionHistory(user1, "01/08/2016", "31/08/2016");
		f.test_TransactionHistory(user2, "01/08/2016", "31/08/2016");
		f.test_TransactionHistory(user3, "01/08/2016", "31/08/2016");
		f.test_TransactionHistory(user4, "01/08/2016", "31/08/2016");
		
		// Confirm balance: user 1 Rp.   850,000
		// Confirm balance: user 2 Rp. 4,950,000
		// Confirm balance: user 3 Rp.    70,000
		// Confirm balance: user 4 Rp.    30,000
		f.test_CheckBalance(user1, FWUtil.PARAM_SALDO, "850000");
		f.test_CheckBalance(user2, FWUtil.PARAM_SALDO, "4950000");
		f.test_CheckBalance(user3, FWUtil.PARAM_SALDO, "70000");
		f.test_CheckBalance(user4, FWUtil.PARAM_SALDO, "30000");
		
		// Delete user1, user2, user3, user4
		f.test_ForceCloseAccount(user1);
		f.test_ForceCloseAccount(user2);
		f.test_ForceCloseAccount(user3);
		f.test_ForceCloseAccount(user4);
	}
	
	/*****************
	 * Invalid identifier
	 *****************/	
	private static void test_frontend_case5(FWFrontend f, Logger logger) {
		
		FWUtil.log(logger, "\n------------- TEST CASE 5 START -------------");
		
		String myName	= "TesterZ01";
		String myEmail	= "tester.z01@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP   = "";
		String myPIN 	= "000000";		
		FWUser user1 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);
		FWUser user2 = new FWUser("", "TesterZ02", "tester.z02@yahoo.com", "19/07/1980", "081360000002", "000000");
		
		String NOHP_INVALID = "X+81360000004";
		String NOHP_VALID = "081360000004";
		String NOHP_VALID_WITH_PLUS = "+6281360000004";
		String ACCTID_INVALID = "123XY+78";
		String ACCTID_NOTFOUND = "12345678";
		
		// registration: missing nohp
		user1.accountID = f.test_Registration(user1, FWUtil.PARAM_RESULTCODE, "300");
		// registration: invalid nohp
		user1.nohp		= NOHP_INVALID;
		user1.accountID = f.test_Registration(user1, FWUtil.PARAM_RESULTCODE, "345");
		// registration: valid nohp
		user1.nohp		= NOHP_VALID;
		user1.accountID = f.test_Registration(user1);
		// close test account
		f.test_CloseAccount(user1);
		// registration: valid nohp with +
		user1.nohp		= NOHP_VALID_WITH_PLUS;
		user1.accountID = f.test_Registration(user1);
		// close test account
		f.test_CloseAccount(user1);
		

		// set user as registered: missing accountid
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		f.test_UpgradeUserToRegistered(user1, FWUtil.PARAM_RESULTCODE, "500");
		// set user as registered: accountid not found
		user1.accountID = "12345678";
		f.test_UpgradeUserToRegistered(user1, FWUtil.PARAM_RESULTCODE, "51");
		// set user as registered: invalid accountid
		user1.accountID = ACCTID_INVALID;
		f.test_UpgradeUserToRegistered(user1, FWUtil.PARAM_RESULTCODE, "515");		
		// set user as registered: valid accountid
		user1.accountID = f.test_Registration(user1);
		f.test_UpgradeUserToRegistered(user1);
		// close test account
		f.test_CloseAccount(user1);		

		// set user as registered: missing accountid
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		f.test_UpgradeUserToMerchant(user1, FWUtil.PARAM_RESULTCODE, "500");
		// set user as registered: accountid not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_UpgradeUserToMerchant(user1, FWUtil.PARAM_RESULTCODE, "51");
		// set user as registered: invalid accountid
		user1.accountID = ACCTID_INVALID;
		f.test_UpgradeUserToMerchant(user1, FWUtil.PARAM_RESULTCODE, "515");	
		// set user as registered: valid accountid
		user1.accountID = f.test_Registration(user1);
		f.test_UpgradeUserToMerchant(user1);
		// close test account
		f.test_CloseAccount(user1);		
		
		// check balance: missing accountid
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		f.test_CheckBalance(user1, FWUtil.PARAM_RESULTCODE, "500");
		// check balance: accountid not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_CheckBalance(user1, FWUtil.PARAM_RESULTCODE, "51");
		// check balance: invalid accountid
		user1.accountID = ACCTID_INVALID;
		f.test_CheckBalance(user1, FWUtil.PARAM_RESULTCODE, "515");	
		// check balance: valid accountid
		user1.accountID = f.test_Registration(user1);
		f.test_CheckBalance(user1);
		// close test account
		f.test_CloseAccount(user1);		
		

		// change phone number: missing accountid
		String NEWNOHP_VALID = NOHP_VALID + "9";
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		f.test_ChangePhoneNumber(user1, NEWNOHP_VALID, FWUtil.PARAM_RESULTCODE, "500");
		// change phone number: accountid not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_ChangePhoneNumber(user1, NEWNOHP_VALID, FWUtil.PARAM_RESULTCODE, "51");		
		// change phone number: invalid accountid
		user1.accountID = ACCTID_INVALID;
		f.test_ChangePhoneNumber(user1, NEWNOHP_VALID, FWUtil.PARAM_RESULTCODE, "515");
		// change phone number: valid accountid
		user1.accountID = f.test_Registration(user1);
		f.test_ChangePhoneNumber(user1, NEWNOHP_VALID);
		// change phone number: missing nohp)
		f.test_ChangePhoneNumber(user1, "", FWUtil.PARAM_RESULTCODE, "315");
		// change phone number: invalid nohp
		f.test_ChangePhoneNumber(user1, NOHP_INVALID, FWUtil.PARAM_RESULTCODE, "346");
		// change phone number: valid nohp
		f.test_ChangePhoneNumber(user1, NOHP_VALID);
		// change phone number: same valid nohp, but with plus
		f.test_ChangePhoneNumber(user1, NOHP_VALID_WITH_PLUS, FWUtil.PARAM_RESULTCODE, "973");
		// change phone number: valid nohp with +
		f.test_ChangePhoneNumber(user1, "+6299989998999");
		// close test account
		f.test_CloseAccount(user1);			

		
		// transfer p2p: accountid sender missing
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		user2.accountID = f.test_Registration(user2);
		f.test_TransferP2P(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "500"); 
		// transfer p2p: accountid sender not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_TransferP2P(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "51"); 
		// transfer p2p: accountid sender invalid
		user1.accountID = ACCTID_INVALID;
		f.test_TransferP2P(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "515"); 
		// transfer p2p: accountid recipient missing
		user1.accountID = "";
		f.test_TransferP2P(user2, user1, "10000", FWUtil.PARAM_RESULTCODE, "316"); 	
		// transfer p2p: accountid recipient not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_TransferP2P(user2, user1, "10000", FWUtil.PARAM_RESULTCODE, "512"); 	
		// transfer p2p: accountid recipient invalid		
		user1.accountID = ACCTID_INVALID;
		f.test_TransferP2P(user2, user1, "10000", FWUtil.PARAM_RESULTCODE, "317"); 
		// transfer p2p: missing amount
		user1.accountID = f.test_Registration(user1);
		f.test_TransferP2P(user1, user2, "", FWUtil.PARAM_RESULTCODE, "318");
		// transfer p2p: invalid amount
		f.test_TransferP2P(user1, user2, "12XY+00", FWUtil.PARAM_RESULTCODE, "319");
		// close test account
		f.test_CloseAccount(user1);
		f.test_CloseAccount(user2);	

		
		// transfer p2m: accountid sender missing
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		user2.accountID = f.test_Registration(user2);
		f.test_UpgradeUserToMerchant(user2);
		f.test_TransferP2M(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "500");
		// transfer p2m: accountid sender not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_TransferP2M(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "51");
		// transfer p2m: accountid sender invalid
		user1.accountID = ACCTID_INVALID;
		f.test_TransferP2M(user1, user2, "10000", FWUtil.PARAM_RESULTCODE, "515");
		// transfer p2m: accountid recipient missing
		user1.accountID = "";
		f.test_TransferP2M(user2, user1, "10000", FWUtil.PARAM_RESULTCODE, "316");	
		// transfer p2m: accountid recipient not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_TransferP2M(user2, user1, "10000", FWUtil.PARAM_RESULTCODE, "512"); 	
		// transfer p2m: accountid recipient invalid		
		user1.accountID = ACCTID_INVALID;
		f.test_TransferP2M(user2, user1, "10000", FWUtil.PARAM_RESULTCODE, "317"); 
		// transfer p2m: missing amount
		user1.accountID = f.test_Registration(user1);
		f.test_TransferP2M(user1, user2, "", FWUtil.PARAM_RESULTCODE, "318"); 
		// transfer p2p: invalid amount
		f.test_TransferP2M(user1, user2, "12XY+00", FWUtil.PARAM_RESULTCODE, "319"); 
		// close test account
		f.test_CloseAccount(user1);
		f.test_CloseAccount(user2);	
	

		// transfer bank: accountid sender missing
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		f.test_TransferBankNowExec(user1, "100000", "013", "101010", FWUtil.PARAM_RESULTCODE, "500", "500");
		// transfer bank: accountid sender not found		
		user1.accountID = ACCTID_NOTFOUND;
		f.test_TransferBankNowExec(user1, "100000", "013", "101010", FWUtil.PARAM_RESULTCODE, "51", "51");
		// transfer bank: accountid sender invalid
		user1.accountID = ACCTID_INVALID;
		f.test_TransferBankNowExec(user1, "100000", "013", "101010", FWUtil.PARAM_RESULTCODE, "515", "515");		
		// transfer bank: amount missing
		user1.accountID = f.test_Registration(user1);
		f.test_UpgradeUserToRegistered(user1);
		f.test_TransferBankNowExec(user1, "", "013", "101010", FWUtil.PARAM_RESULTCODE, "318", "318");
		// transfer bank: amount invalid
		f.test_TransferBankNowExec(user1, "12XY+00", "013", "101010", FWUtil.PARAM_RESULTCODE, "319", "319");
		// transfer bank: bankid missing
		f.test_TransferBankNowExec(user1, "10000", "", "101010", FWUtil.PARAM_RESULTCODE, "332", "332");
		// transfer bank: bankid not found
		f.test_CashIn(user1, "20000");
		f.test_TransferBankNowExec(user1, "10000", "999", "101010", FWUtil.PARAM_RESULTCODE, "976", "976");
		// close test account
		f.test_ForceCloseAccount(user1);

 
		// topup pulsa: accountid missing
		user1.accountID = "";
		user1.nohp = NOHP_VALID;
		f.test_TopupPulsa(user1, "10000", "081308130813", FWUtil.PARAM_RESULTCODE, "500");
		// topup pulsa: accountid not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_TopupPulsa(user1, "10000", "081308130813", FWUtil.PARAM_RESULTCODE, "51");
		// topup pulsa: accountid invalid
		user1.accountID = ACCTID_INVALID;
		f.test_TopupPulsa(user1, "10000", "081308130813", FWUtil.PARAM_RESULTCODE, "515");		
		// topup pulsa: amount missing
		user1.accountID = f.test_Registration(user1);
		f.test_TopupPulsa(user1, "", "081308130813", FWUtil.PARAM_RESULTCODE, "318");
		// topup pulsa: amount invalid
		f.test_TopupPulsa(user1, "12XY+00", "081308130813", FWUtil.PARAM_RESULTCODE, "319");
		// topup pulsa: amount denomination not available
		f.test_TopupPulsa(user1, "12500", "081308130813", FWUtil.PARAM_RESULTCODE, "931");
		// topup pulsa: nohpdest missing
		f.test_TopupPulsa(user1, "10000", "", FWUtil.PARAM_RESULTCODE, "300"); // By design, error code 300, not 315
		// topup pulsa: nohpdest invalid
		f.test_TopupPulsa(user1, "10000", "0813A+C!0813", FWUtil.PARAM_RESULTCODE, "345");
		// topup pulsa: nohpdest with +
		f.test_TopupPulsa(user1, "10000", NOHP_VALID_WITH_PLUS, FWUtil.PARAM_RESULTCODE, "955");
		// close test account
		f.test_CloseAccount(user1);

		// transaction history: missing accountid
		user1.accountID = "";
		f.test_TransactionHistory(user1, "01/08/2016", "31/08/2016", FWUtil.PARAM_RESULTCODE, "500");
		// transaction history: accountid not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_TransactionHistory(user1, "01/08/2016", "31/08/2016", FWUtil.PARAM_RESULTCODE, "51"); 
		// transaction history: invalid accountid
		user1.accountID = ACCTID_INVALID;
		f.test_TransactionHistory(user1, "01/08/2016", "31/08/2016", FWUtil.PARAM_RESULTCODE, "515");	
		// transaction history: valid accountid
		user1.nohp = NOHP_VALID;
		user1.accountID = f.test_Registration(user1);
		f.test_TransactionHistory(user1, "01/08/2016", "31/08/2016");
		// transaction history: startdate missing
		f.test_TransactionHistory(user1, "", "31/08/2016", FWUtil.PARAM_RESULTCODE, "321");
		// transaction history: startdate invalid format
		f.test_TransactionHistory(user1, "01/28/2016", "31/08/2016", FWUtil.PARAM_RESULTCODE, "322");
		// transaction history: startdate not numeric
		f.test_TransactionHistory(user1, "1X/08/2016", "31/08/2016", FWUtil.PARAM_RESULTCODE, "322");
		// transaction history: enddate missing
		f.test_TransactionHistory(user1, "01/08/2016", "", FWUtil.PARAM_RESULTCODE, "323");
		// transaction history: enddate invalid format
		f.test_TransactionHistory(user1, "01/08/2016", "01/28/2016", FWUtil.PARAM_RESULTCODE, "324");
		// transaction history: enddate not numeric
		f.test_TransactionHistory(user1, "01/08/2016", "1X/08/2016", FWUtil.PARAM_RESULTCODE, "324");
		// transaction history: enddate is less than startdate
		f.test_TransactionHistory(user1, "31/08/2016", "01/08/2016", FWUtil.PARAM_RESULTCODE, "348");		
		// close test account
		f.test_CloseAccount(user1);	
		
		// close account: missing accountid
		user1.accountID = "";
		f.test_CloseAccount(user1, FWUtil.PARAM_RESULTCODE, "500");
		// transaction history: accountid not found
		user1.accountID = ACCTID_NOTFOUND;
		f.test_CloseAccount(user1, FWUtil.PARAM_RESULTCODE, "51");
		// transaction history: invalid accountid
		user1.accountID = ACCTID_INVALID;
		f.test_CloseAccount(user1, FWUtil.PARAM_RESULTCODE, "515");
		// close account: valid accountid
		user1.accountID = f.test_Registration(user1);
		f.test_CloseAccount(user1);
		
	}

	/*****************
	 * Topup pulsa, Transaction History
	 *****************/	
	private static void test_frontend_case6(FWFrontend f, Logger logger) {
		
		FWUtil.log(logger, "\n------------- TEST CASE 6 START -------------");
		
		// Create & Register user1 (unregistered), user2 (unregistered->registered), user3 (unregistered->merchant)
		String myName	= "TesterZ01";
		String myEmail	= "tester.z01@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP 	= "081352000011";
		String myPIN 	= "000000";		
		FWUser user1 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user1.accountID = f.test_Registration(user1);
		
		myName	= "TesterZ02";
		myEmail	= "tester.z02@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081352000012";
		myPIN 	= "000000";		
		FWUser user2 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user2.accountID = f.test_Registration(user2);

		myName	= "TesterZ03";
		myEmail	= "tester.z03@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081352000013";
		myPIN 	= "000000";		
		FWUser user3 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user3.accountID = f.test_Registration(user3);		
		
		// Upgrade to Registered user2
		f.test_UpgradeUserToRegistered(user2);
		// Upgrade to Merchant user3
		f.test_UpgradeUserToMerchant(user3);

		// Top up balance: Rp   900,000 ----- manual: user1
		f.test_CashIn(user1, "900000");
		// Top up balance: Rp 4,000,000 ----- manual: user2
		f.test_CashIn(user2, "4000000");

		// P2P transfer user2 -> user3: Rp 100,000
		f.test_TransferP2P(user2, user3, "100000");
				
		// Topup pulsa 081210001000 user1: Rp. 10,000
		// Topup pulsa 081210001000 user2: Rp. 10,000
		// Topup pulsa 081210001000 user3: Rp. 10,000
		f.test_TopupPulsa(user1, "10000", "081210001000");
		f.test_TopupPulsa(user2, "10000", "081210001000");
		f.test_TopupPulsa(user3, "10000", "081210001000");
		
		// Confirm balance
		f.test_CheckBalance(user1, FWUtil.PARAM_SALDO,  "888700"); 
		f.test_CheckBalance(user2, FWUtil.PARAM_SALDO, "3888700"); 
		f.test_CheckBalance(user3, FWUtil.PARAM_SALDO,   "88700");
	
		// Delete user1, user2, user3
		f.test_ForceCloseAccount(user1);
		f.test_ForceCloseAccount(user2);
		f.test_ForceCloseAccount(user3);
	}		

	/*****************
	 * Bank Transfer, Topup pulsa, Transaction History
	 *****************/	
	private static void test_frontend_case7(FWFrontend f, Logger logger) {
		
		FWUtil.log(logger, "\n------------- TEST CASE 7 START -------------");
		
		// Create & Register user1 (unregistered), user2 (unregistered->registered), user3 (unregistered->merchant)
		String myName	= "TesterZ01";
		String myEmail	= "tester.z01@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP 	= "081352000011";
		String myPIN 	= "000000";		
		FWUser user1 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user1.accountID = f.test_Registration(user1);
		
		myName	= "TesterZ02";
		myEmail	= "tester.z02@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081352000012";
		myPIN 	= "000000";		
		FWUser user2 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user2.accountID = f.test_Registration(user2);

		myName	= "TesterZ03";
		myEmail	= "tester.z03@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "081352000013";
		myPIN 	= "000000";		
		FWUser user3 = new FWUser("", myName, myEmail, myDOB, myNoHP, myPIN);		
		user3.accountID = f.test_Registration(user3);		
		
		// Upgrade to Registered user2
		f.test_UpgradeUserToRegistered(user2);
		// Upgrade to Merchant user3
		f.test_UpgradeUserToMerchant(user3);

		// Top up balance: Rp   900,000 ----- manual: user1
		f.test_CashIn(user1, "900000");
		// Top up balance: Rp 4,000,000 ----- manual: user2
		f.test_CashIn(user2, "4000000");

		// P2P transfer user2 -> user3: Rp 100,000
		f.test_TransferP2P(user2, user3, "100000");
		
		// Bank transfer 013 101010 user1: Rp. 100,000 (expect error: unreg not allowed to transfer)
		//TEMPORARY f.test_TransferBankNowInq(user1, "100000", "013", "101010", FWUtil.PARAM_RESULTCODE, "336");
		//TEMPORARY f.test_TransferBankNowExec(user1, "100000", "013", "101010", FWUtil.PARAM_RESULTCODE, "336", "336");
		// Bank transfer 013 101010 user2: Rp. 100,000
		f.test_TransferBankNowInq(user2, "100000", "013", "101010");
		f.test_TransferBankNowExec(user2, "100000", "013", "101010");
		// Bank transfer 013 101010 user3: Rp. 50,000
		f.test_TransferBankNowExec(user3, "50000", "013", "101010");
				
		// Confirm balance
		f.test_CheckBalance(user1, FWUtil.PARAM_SALDO,  "900000"); 
		f.test_CheckBalance(user2, FWUtil.PARAM_SALDO, "3792500"); 
		f.test_CheckBalance(user3, FWUtil.PARAM_SALDO,   "42500");
	
		// Delete user1, user2, user3
		f.test_ForceCloseAccount(user1);
		f.test_ForceCloseAccount(user2);
		f.test_ForceCloseAccount(user3);
	}	
	

	
	/*****************
	 * Transaction limits
	 *****************/	
	private static void test_frontend_case8(FWFrontend f, FWUser donor1, FWUser donor2, Logger logger) {
		
		
		// Saldo max unreg user = Rp 1 juta
		// Saldo max reg user = Rp 5 juta
		// Saldo max merchant user = Rp 100 juta
		
		// Max transaction value per month unreg user = Rp 20 juta
		// Max transaction value per month reg user = Rp 20 juta
		// Max transaction value per month merchant user = Rp 100 juta		
		
		// Max transaction for merchant
		
		
		
	}	
	
	private static void test_backend(String merchantID, String key, String host, Logger logger) {

		FWBackendAlto w_backend = new FWBackendAlto(merchantID, key, host, logger);				
		
		/*****************
		 * Get Master Data
		 *****************/
		//w_backend.test_GetMasterData();	
					
		/*****************
		 * Registration, Upgrade User to Registered
		 *****************/			
		String myAccountID = "";
		String myName	= "ValerinoW91";
		String myEmail	= "valerino.w91@yahoo.com";
		String myDOB	= "19/07/1980";
		String myNoHP 	= "08120000091";
		String myPIN 	= "000000";	
		FWUser user1 = new FWUser(myAccountID, myName, myEmail, myDOB, myNoHP, myPIN);
		
		myAccountID = "";
		myName	= "ValerinoW02";
		myEmail	= "valerino.w02@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "08120000002";
		myPIN 	= "000000";		
		FWUser user2 = new FWUser(myAccountID, myName, myEmail, myDOB, myNoHP, myPIN);		

		myAccountID = "";
		myName	= "ValerinoW03";
		myEmail	= "valerino.w03@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "08120000003";
		myPIN 	= "000000";		
		FWUser user3 = new FWUser(myAccountID, myName, myEmail, myDOB, myNoHP, myPIN);			

		myAccountID = "";
		myName	= "ValerinoW04";
		myEmail	= "valerino.w04@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "08120000004";
		myPIN 	= "000000";		
		FWUser user4 = new FWUser(myAccountID, myName, myEmail, myDOB, myNoHP, myPIN);
		
		//myAccountID = "";
		//myName	= "ValerinoW06";
		//myEmail	= "valerino.w06@yahoo.com";
		//myDOB	= "19/07/1980";
		//myNoHP 	= "08120000006";
		//myPIN 	= "000000";		
		//FWUser user6 = new FWUser(myAccountID, myName, myEmail, myDOB, myNoHP, myPIN);
		
		myAccountID = "";
		myName	= "ValerinoW08";
		myEmail	= "valerino.w08@yahoo.com";
		myDOB	= "19/07/1980";
		myNoHP 	= "08120000008";
		myPIN 	= "000000";		
		FWUser user8 = new FWUser(myAccountID, myName, myEmail, myDOB, myNoHP, myPIN);	// merchant user
		
		//w_backend.test_Registration(user1);
		//w_backend.test_Registration(user2);
		//w_backend.test_Registration(user3);
		//w_backend.test_Registration(user4);
		//w_backend.test_Registration(user6);
		//w_backend.test_Registration(user8);

		// TODO: capture the current account ID
		user1.accountID = "200160001608";
		//user2.accountID = "200160001585";
		//user3.accountID = "200160001586";
		//user4.accountID = "200160001587";
		//user6.accountID = "200160001602";
		//user8.accountID = "200160001604";

		//w_backend.test_UpgradeUserToRegistered(user1);
		//w_backend.test_UpgradeUserToRegistered(user2);
		//w_backend.test_UpgradeUserToRegistered(user3);
		//w_backend.test_UpgradeUserToRegistered(user4);
		
		//w_backend.test_UpgradeUserToMerchant(user8);
		//w_backend.test_UpgradeUserToRegistered(user8); // expect error

		/*****************
		 * Login (optional)
		 *****************/
		//w_backend.test_Login(user1);
		//w_backend.test_Login(user2);
		//w_backend.test_Login(user3);
		//w_backend.test_Login(user4);
		//w_backend.test_Login(user6);
		//w_backend.test_Login(user8);
		
		/*****************
		 * P2P
		 *****************/
		/*
		w_backend.test_CheckBalance(user1);
		w_backend.test_CheckBalance(user2);
		w_backend.test_CheckBalance(user3);
		w_backend.test_CheckBalance(user4);
		
		w_backend.test_TransferP2P(user1, user4, "10000");
		
		w_backend.test_CheckBalance(user1);
		w_backend.test_CheckBalance(user4);	

		w_backend.test_TransferP2P(user4, user1, "10000");		
		w_backend.test_TransferP2P(user2, user1, "99900"); // expect error: over limit		
		*/
		
		/*****************
		 * Transaction History
		 *****************/
		/*
		w_backend.test_TransactionHistory(user4, "01/08/2016", "02/08/2016");		
		*/
		
		/*****************
		 * Bank Transfer
		 *****************/
		//w_backend.test_TransferBankRealTime(user3, "110000", "145", "101010");
		//w_backend.test_CheckBalance(user3);
		//w_backend.test_TransactionHistory(user3, "01/08/2016", "02/08/2016");		
		
		/*****************
		 * Change Phone Number
		 *****************/
		//w_backend.test_ChangePhoneNumber(user3, "081200000030");

		/*****************
		 * Change PIN
		 *****************/
		//w_backend.test_ChangePIN(user4, "000000");
		
		/*****************
		 * Close Account
		 *****************/		
		//w_backend.test_CloseAccount(user6);
		//w_backend.test_Login(user6);  //TODO Check "Status Anda tidak aktif","resultcode":-3 ?
	   
		/*****************
		 * Top up Pulsa
		 *****************/
		w_backend.test_TopupPulsa(user1, "10000", "081210001000");
		w_backend.test_TopupPulsa(user1, "10000", "6281210001000");
		w_backend.test_TopupPulsa(user1, "10000", "6181210001000");	
		
	}
	
	/* SAMPLE OUTPUT
	 
	REGISTRATION
	Sending 'POST' request to URL Alto : http://202.158.48.243:8899/mcpayment/api/reg_customer.jsp
	Post parameters : org.apache.http.client.entity.UrlEncodedFormEntity@c5d53c3
	Response Code : HTTP/1.1 200 OK
	{"resultmsg":"registration success","resultcode":1,"accountid":"200160001604"}
	Execution time (ms): 558.678

	TRANSFER BANK REALTIME (INQ)
	Sending 'POST' request to URL Alto : http://202.158.48.243:8899/mcpayment/api/services.jsp
	Post parameters : org.apache.http.client.entity.UrlEncodedFormEntity@2dec0883
	Response Code : HTTP/1.1 200 OK
	{"amount":"110000","fee":"7500","namedest":"MAS JAMBRONG","resultmsg":"inquiry succeed","resultcode":100,"datetime":"02\/08\/2016 22:44:39"}
	Execution time (ms): 4984.303

	TRANSFER BANK REALTIME (TRANS)
	Sending 'POST' request to URL Alto : http://202.158.48.243:8899/mcpayment/api/services.jsp
	Post parameters : org.apache.http.client.entity.UrlEncodedFormEntity@5b2326e4
	Response Code : HTTP/1.1 200 OK
	{"trxid":"50","amount":"110000","namedest":"MAS JAMBRONG","bankdest":"Bank Nusantara Parahyangan","resultmsg":"transaction succeed","resultcode":100,"datetime":"02\/08\/2016 22:44:44"}
	Execution time (ms): 9090.337

	CHECK BALANCE
	Sending 'POST' request to URL Alto : http://202.158.48.243:8899/mcpayment/api/services.jsp
	Post parameters : org.apache.http.client.entity.UrlEncodedFormEntity@70c64bc2
	Response Code : HTTP/1.1 200 OK
	{"product":"MC Payment2","name":"ValerinoW03 ","resultmsg":"ok","resultcode":100,"datetime":"02\/08\/2016 22:44:53","accountid":"200160001586","saldo":"4882500"}
	Execution time (ms): 405.854

	CHECK TRANSACTION HISTORY
	Sending 'POST' request to URL Alto : http://202.158.48.243:8899/mcpayment/api/services.jsp
	Post parameters : org.apache.http.client.entity.UrlEncodedFormEntity@fe11c0e
	Response Code : HTTP/1.1 200 OK
	{"resultmsg":"get list transaction succeed","trxlist":[{"trxid":"50","trxdatetime":"02\/08\/2016 16:54:49","trxop":"DB","trxdescr":"transfer ke Bank Nusantara Parahyangan (C78D105843E43E04)","trxamount":"117500","saldo":"4882500","accountidpartner":"0"},{"trxid":"42","trxdatetime":"02\/08\/2016 16:14:30","trxop":"CR","trxdescr":"setor tunai Bank International Indonesia","trxamount":"5000000","saldo":"5000000","accountidpartner":"16"}],"resultcode":100}
	Execution time (ms): 418.52

	*/
	
	/*
	TOPUP PULSA (INQ)
	Sending 'POST' request to URL : http://202.158.48.243:8899/mcpayment/api/services.jsp
	{"amount":"10000","price":"11300","resultmsg":"inquiry succeed","resultcode":100,"hpdest":"081210001000","datetime":"09\/08\/2016 00:05:37"}
	Execution time (ms): 863.204

	TOPUP PULSA (TRANS)
	Sending 'POST' request to URL : http://202.158.48.243:8899/mcpayment/api/services.jsp
	{"trxid":"202","amount":"10000","price":"11300","resultmsg":"transaction succeed","resultcode":100,"hpdest":"081210001000","datetime":"09\/08\/2016 00:05:38"}
	Execution time (ms): 8477.262

	TOPUP PULSA (INQ)
	Sending 'POST' request to URL : http://202.158.48.243:8899/mcpayment/api/services.jsp
	{"amount":"10000","price":"11300","resultmsg":"inquiry succeed","resultcode":100,"hpdest":"081210001000","datetime":"09\/08\/2016 00:05:46"}
	Execution time (ms): 508.466

	TOPUP PULSA (TRANS)
	Sending 'POST' request to URL : http://202.158.48.243:8899/mcpayment/api/services.jsp
	{"trxid":"203","amount":"10000","price":"11300","resultmsg":"transaction succeed","resultcode":100,"hpdest":"081210001000","datetime":"09\/08\/2016 00:05:47"}
	Execution time (ms): 8492.198

	TOPUP PULSA (INQ)
	Sending 'POST' request to URL : http://202.158.48.243:8899/mcpayment/api/services.jsp
	{"resultmsg":"operator tidak diketahui","resultcode":-3}
	Execution time (ms): 199.942

	TOPUP PULSA (TRANS)
	Sending 'POST' request to URL : http://202.158.48.243:8899/mcpayment/api/services.jsp
	{"resultmsg":"operator tidak diketahui","resultcode":-3}
	Execution time (ms): 231.952 
	 */


}


