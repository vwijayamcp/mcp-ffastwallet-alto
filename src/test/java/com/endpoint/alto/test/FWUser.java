package com.endpoint.alto.test;

public class FWUser {
	
	public String accountID;
	public String name;
	public String email;
	public String dob;
	public String nohp;
	public String PIN;

	public FWUser(String accountID, String name, String email, String dob, String nohp, String PIN) {
		this.accountID = accountID;
		this.name = name;
		this.email = email;
		this.dob = dob;
		this.nohp = nohp;
		this.PIN = PIN;
	} 
}
